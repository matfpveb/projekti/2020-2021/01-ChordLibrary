### DEV

build-dev: # reset
	cd client && $(MAKE) build-dev
	cd server && $(MAKE) build

run-dev: build-dev
	docker-compose -f docker-compose.yml up

list-all-containers:
	docker ps -a

stop-all-containers:
	docker stop -f $$(docker ps -a -q)

delete-all-containers:
	docker rm -f $$(docker ps -a -q)

list-all-images:
	docker images -a

delete-all-images:
	docker-compose down --volumes
	docker rmi -f $$(docker images -a -q)

list-all-volumes:
	docker volume ls

delete-all-volumes:
	docker volume rm -f $$(docker volume ls)

reset: stop-all-containers delete-all-containers delete-all-images