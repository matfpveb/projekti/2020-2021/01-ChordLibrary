const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const User = new Schema(
    {
        _id: mongoose.Schema.Types.ObjectId,
        username: { type: String, required: true, unique: true },
        password: { type: String, required: true },
        admin: { type: Boolean, default: false },
    },
    { timestamps: true }
);

module.exports = mongoose.model('user', User);
