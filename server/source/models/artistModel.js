const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Artist = new Schema(
    {
        _id: mongoose.Schema.Types.ObjectId,
        artistName: { type: String, required: true },
        songName: { type: String, required: true },
        content: { type: String, required: true }
    },
    { timestamps: true }
);

module.exports = mongoose.model('artists', Artist);
