const express = require('express');
const jwt = require('jsonwebtoken')
const ArtistController = require('../controllers/artistController');
const checkToken = require('../functions/validation/token');

const router = express.Router();

router.post('/artists', ArtistController.postArtists);
router.post('/artist', ArtistController.createArtist);
router.get('/artist/:artist', ArtistController.getArtist);
router.get('/artists/:letter', ArtistController.getArtists);
router.delete('/artists', ArtistController.deleteArtists);
module.exports = router;

