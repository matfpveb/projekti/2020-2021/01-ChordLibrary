const express = require('express');

const ChordsController = require('../controllers/chordsController');

const router = express.Router();

router.get('/chords/:songID', ChordsController.getChords);

module.exports = router;
