const express = require('express');

const LettersController = require('../controllers/lettersController');

const router = express.Router();

router.get('/letters', LettersController.getLetters);

module.exports = router;
