const express = require('express');

const UserController = require('../controllers/userController');
const checkToken = require('../functions/validation/token');

const router = express.Router();

router.post('/user/login', UserController.loginUser);
router.post('/user/register', UserController.registerUser);
router.get('/user', UserController.getUsers);
router.delete('/user', UserController.deleteUser);
router.get('/user/loaduser', checkToken, UserController.loadUser);

module.exports = router;
