const express = require('express');

const LastAddedSongsController = require('../controllers/lastAddedSongsController');

const router = express.Router();

router.get('/lastAddedSongs', LastAddedSongsController.getLastAddedSongs);

module.exports = router;
