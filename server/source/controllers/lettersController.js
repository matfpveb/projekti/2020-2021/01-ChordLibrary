const getLetters = (req, res) => {
    // ********************** TODO **********************

    const hardcoded = [
        'A',
        'B',
        'C',
        'D',
        'E',
        'F',
        'G',
        'H',
        'I',
        'J',
        'K',
        'L',
        'M',
        'N',
        'O',
        'P',
        'R',
        'S',
        'T',
        'U',
        'V',
        'Y',
        'Z',
    ];
    res.status(200).json(hardcoded);
};

module.exports = {
    getLetters,
};
