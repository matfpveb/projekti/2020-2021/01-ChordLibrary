const mongoose = require('mongoose');
const db = require('../db/index');
const bcrypt = require('bcrypt');
const Artist = require('../models/artistModel');
const songsJSON = require('../dataForDBCreate/allSongsFromPesmarica.json')
const cripting = require('../functions/cripting/cripting');

const createArtist = (req, res) => {
    const artistName = req.body.artistName;
    const songName = req.body.songName;
    const content = req.body.content;
    console.log(artistName);
    const newArtist = new Artist({
        _id: new mongoose.Types.ObjectId(),
        artistName: artistName,
        songName: songName,
        content: content,
    });

    newArtist.save((err, res) => {
        console.log(res);
        if (err) return res.status(400).json();
        console.log(`${newArtist} inserted succussfully!`);
    });

    return res.status(201).json({});
};

const getArtist = (req, res) => {
    const artistID = req.params.artist;
   /* 
    const hardcoded = {
        artistName: 'Artist name',
        artistID: 0,
        songs: hardcodedSongs,
        numberOfSongs: hardcodedSongs.length,
        about:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    };*/
    const artistName = cripting.recreateArtistID(artistID).trim();

    Artist.find({artistName}, (err, docs)=> {
        if(err) return res.status(404).json(err);

        let songs = [];
        for(let i=0; i<docs.length; ++i){
            let s = {
                songName: docs[i].songName,
                songID: docs[i]._id,
            }
            songs.push(s);

        }   

        let obj = {
            artistName,
            artistID,
            songs,
            numberOfSongs: songs.length,
            about: "Nesto sasvim izvesno",
        }
        return res.status(200).json(obj);
    })
};

const getArtists = async (req, res) => {
    const { letter } = req.params;
    /*const hardcoded = [
        {
            artistName: 'Very long artist name',
            artistID: 0,
            numberOfSongs: 24,
        },
    ];*/

    Artist.find({"artistName" :{$regex: "^"+letter, $options: 'i' }}, (err, person) => {
            if (err) return res.status(404).json(err);

            let objects = []

            for(let i=0; i<person.length; ++i){
                let p = {
                    artistName: person[i].artistName,
                    artistID: cripting.createArtistID(person[i].artistName),
                }
                if(objects.length !== 0 && person[i].artistName === person[i-1].artistName)
                    continue
                objects.push(p);

            }   
            
            return res.status(200).json(objects);
        });
};

const postArtists = async (req, res) => {
    /*const Bajaga = new Artist({
        _id: new mongoose.Types.ObjectId(),
        artistName: 'Bajaga',
        songName: 'Tisina',
        content: `
Am               C 
Mrak se skupio u kap 
                G 
Rano jutro kao slap 
        Am 
Ulazi u sobu 
`,
    });

    Bajaga.save((err, res) => {
        if (err) return console.error(err);
        console.log(`${Bajaga} inserted succussfully!`);
    });

    const CrvenaJabuka = new Artist({
        _id: new mongoose.Types.ObjectId(),
        artistName: 'Crvena Jabuka',
        songName: 'To mi radi',
        content: `
E           C#m     A             H
volim te djevojcice dok se nebo zatvara
E           C#m         A          H
neka vriste nase ulice neka krene zabava
        E            C#m        A         H
bolja buducnost nije htjela da nas da saceka
        E              C#m        A         H
pridji blize da vidim tajnu koju cuvas u ocima
ref.
        E         C#m       A             H
to mi radi to mi radi jer ti mozes da mi radis sve
        E          C#m      A           H
to mi radi to mi radi jer ja zaljubio sam se
`,
    });

    CrvenaJabuka.save((err, res) => {
        if (err) return console.error(err);
        console.log(`${CrvenaJabuka} inserted succussfully!`);
    });

    const ParniValjak = new Artist({
        _id: new mongoose.Types.ObjectId(),
        artistName: 'Parni valjak',
        songName: 'Dodji',
        content: `
F#m     E      C#m       D    C#      F#m
Dođi, zaboravi, nudim ti noći čarobne
    E       D        C#        F#m
i buđenja u postelji punoj šecera
`,
    });

    ParniValjak.save((err, res) => {
        if (err) return console.error(err);
        console.log(`${ParniValjak} inserted succussfully!`);
    });

    const DinoMerlin = new Artist({
        _id: new mongoose.Types.ObjectId(),
        artistName: 'Dino Merlin',
        songName: 'Kremen',
        content: `
Am
ti, prosto zadrhtim kad te ugledam
G
u dah mi se pretvori glas
Am
ti, sve ti oprostim, sto i ne trebam
G
jer cuvam jos nesto za nas
`,
    });

    DinoMerlin.save((err, res) => {
        if (err) return console.error(err);
        console.log(`${DinoMerlin} inserted succussfully!`);
    });

    const DarkoLazic = new Artist({
        _id: new mongoose.Types.ObjectId(),
        artistName: 'Darko Lazic',
        songName: 'Brate moj',
        content: `
Bm     F#      C#    G#      
Petak je vece, ja kuci sedim sam
vrtim kanale po ceo dan
telefon zvoni, znam dobro ja taj broj
F#   G#     Bm
drugovi to me zovu sad
`,
    });

    DarkoLazic.save((err, res) => {
        if (err) return console.error(err);
        console.log(`${DarkoLazic} inserted succussfully!`);
    });*/

    //const nesto = JSON.parse(songsJSON);
    //console.log(songsJSON.A);
    
    for(let artist of songsJSON){
        for(let song of artist.songs){
            const newArtist = new Artist({
                _id: new mongoose.Types.ObjectId(),
                artistName: artist.artistName,
                songName: song.songName,
                content: song.content,
            });
        
            await newArtist.save((err, result) => {
                console.log("*****************1")
                console.log(result);
                console.log("*****************2")
                console.log(err);
                console.log("*****************3")
                console.log(`${newArtist} inserted succussfully!`);
            });
        }
    }
    return res.status(201).json("songsJSON");
};

const deleteArtist = async (_id) => {
    Artist.deleteOne({_id}, (err) => {
        if (err) console.log(err);
        console.log('delete');
    });
};

const deleteArtists = async (req, res) => {
    Artist.find((err, docs)=> {
        if(err) return res.status(400).json(err);

        for(let i=0; i<docs.length; ++i){
            deleteArtist(docs[i]._id);
        }
        return res.status(200).json("Success");
    })
};

module.exports = {
    createArtist,
    getArtist,
    getArtists,
    deleteArtist,
    postArtists,
    deleteArtists
};
