const Artist = require('../models/artistModel');
const mongoose = require('mongoose');
const cripting = require('../functions/cripting/cripting');

const getChords = (req, res) => {
    const _id = req.params.songID;
    
    const hardcoded = {
        artistName: 'Artist name',
        artistID: 0,
        songName: 'Song name',
        songID: 100,
        content: JSON.stringify(`A                         F                         C                                G`),
    };
    
    Artist.findOne({_id}, (err, song)=> {
        if(err) return res.status(400).json(err);

        let obj = {
            artistName: song.artistName,
            artistID: cripting.createArtistID(song.artistName),
            songName: song.songName,
            songID: song._id,
            content: JSON.stringify(song.content),
        }
        console.log(obj.content)
        return res.status(200).json(obj);
    });
    
};

module.exports = {
    getChords,
};
