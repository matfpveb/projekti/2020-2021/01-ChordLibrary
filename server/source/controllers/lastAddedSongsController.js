const Artist = require('../models/artistModel');

const getLastAddedSongs = async (req, res) => {
    
    const sorted = await Artist.find().sort({createdAt:-1}).limit(5);
    const valueToReturn = sorted.map((song)=>{
        return{
            artistName: song.artistName,
            songName: song.songName,
            songID: song._id,
        }
    });
    res.status(200).json(valueToReturn);
};

module.exports = {
    getLastAddedSongs,
};
