const mongoose = require('mongoose');
const db = require('../db/index');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const User = require('../models/userModel');
const jwt = require('jsonwebtoken');

loginUser = async (req, res) => {
    const username = req.body.username;

    await User.findOne({ username }, async (err, user) => {
        if (err) return res.status(400).json('err');

        if (user === null) return res.status(404).json("User doesn't exist");
        const match = await bcrypt.compare(req.body.password, user.password);

        if (match) {
            const payload = { username: user.username, admin: user.admin };
            const accessToken = jwt.sign(
                payload,
                process.env.ACCESS_TOKEN_SECRET,
                { expiresIn: '20m' }
            );

            const obj = {
                token: accessToken,
                user,
            };

            return res.status(200).json(obj);
        } else {
            return res.status(400).json(err);
        }
    });
};

registerUser = async (req, res) => {
    const username = req.body.username;
    const password = await bcrypt.hash(req.body.password, saltRounds);
    const newUser = new User({
        _id: new mongoose.Types.ObjectId(),
        username: username,
        password: password,
    });

    newUser.save((err, user) => {
        if (err) {
            console.log(err);
            return res.status(400).json(err);
        }

        console.log(`${newUser} inserted succussfully!`);
        const payload = {
            username: username,
            userID: newUser._id,
            admin: newUser.admin,
        };
        const accessToken = jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET, {
            expiresIn: '20m',
        });

        const obj = {
            token: accessToken,
            user,
        };

        return res.status(200).json(obj);
    });
};

loadUser = async (req, res) => {
    return res.status(200).json(req.payload);
};

getUsers = async (req, res) => {
    const users = await User.find();
    return res.status(200).json(users);
};

deleteUser = async (req, res) => {
    await User.deleteOne({ _id: '60a00b15e8c830003f656c84' }, (err) => {
        if (err) return console.error(err);
        console.log('delete');
        return res.status(200).json({ msg: 'Delete successful' });
    });
};

module.exports = {
    getUsers,
    deleteUser,
    registerUser,
    loginUser,
    loadUser,
};
