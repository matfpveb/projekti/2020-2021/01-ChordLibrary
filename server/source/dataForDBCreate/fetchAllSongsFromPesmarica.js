(async () => {
    const fetchDataAndParseDom = async (url) => {
        const resp = await fetch(url);
        const data = await resp.text();
        const parser = new DOMParser();
        const parsedDocument = parser.parseFromString(data, 'text/html');
        return parsedDocument;
    };
    try {
        const homePageDocument = await fetchDataAndParseDom(
            'http://pesmarica.rs/'
        );
        const links = Array.from(
            homePageDocument.querySelectorAll('#ArtistLetterList li a')
        );
        let regexForLetter = /\/Akordi\/(\w)\/0/; // nedostaju latinicna slova i 0-9, istraziti unicode i js
        const letters = links
            .filter((link) => regexForLetter.test(link.href))
            .map((letterLink) => ({
                letter: letterLink.text,
                href: letterLink.href,
            }));
        let artists = {};
        for (const { letter, href } of letters) {
            const documentPage = await fetchDataAndParseDom(href);
            const artistsOnCurrentLetter = Array.from(
                documentPage.querySelectorAll('.content > div > a'),
                (artistLink) => ({
                    artistName: artistLink.text,
                    href: artistLink.href,
                    songs: [],
                })
            );
            artists[letter] = artistsOnCurrentLetter;
        }
        for (let letter in artists) {
            for ({ artistName, href, songs } of artists[letter]) {
                const artistPageDocument = await fetchDataAndParseDom(href);
                Array.from(
                    artistPageDocument.querySelectorAll('.content > div > a'),
                    (songLink) => {
                        return {
                            songName: songLink.text,
                            href: songLink.href,
                            content: '',
                        };
                    }
                ).forEach((song) => {
                    songs.push(song);
                });
                for (let song of songs) {
                    const songPageDocument = await fetchDataAndParseDom(
                        song.href
                    );
                    try {
                        song.content = songPageDocument.querySelector(
                            'pre'
                        ).textContent;
                    } catch (err) {
                        continue;
                    }
                }
            }
        }
        // console.log(artists);
        return JSON.stringify(artists);
    } catch (err) {
        console.log('Greska: ', err.message);
    }
})();
