
function createArtistID (artistName) {
    let appenended = ""
    for(i=0;i<artistName.length;++i){
        appenended += artistName.charCodeAt(i);
        if(i===artistName.length-1){
            break;
        }
        appenended += "-";

    }
    return appenended;
}

function recreateArtistID (artistString){
    let splitted = artistString.split('-');
    let artistName = "";
    for(s of splitted){
        artistName += String.fromCharCode(parseInt(s));
    }

    return artistName;
}

module.exports = {
    createArtistID,
    recreateArtistID,
};