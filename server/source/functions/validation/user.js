
function validate_password(password){
    var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
    return re.test(password);
}

function validate_username(username){
    var re = /\w{6}/;
    return re.test(username);
}
