const jwt = require('jsonwebtoken');
const checkToken = (req, res, next) => {
    const token = req.header('x-auth-token');
    console.log('******************token:', token);
    if (!token) {
        return res.status(401).json('Not a valid token');
    }

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, client) => {
        if (err) throw err;
        console.log(client);
        console.log(client);
        console.log(client);
        const username = client.username;
        const admin = client.admin;
        req.payload = { username, admin };
        req.newToken = jwt.sign(req.payload, process.env.ACCESS_TOKEN_SECRET, {
            expiresIn: '20m',
        });
        next();
    });
};

module.exports = checkToken;
