const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
require('dotenv').config();

const db = require('./db');
const artistRouter = require('./routes/artistRouter');
const userRouter = require('./routes/userRouter');
const lastAddedSongsRouter = require('./routes/lastAddedSongsRouter');
const lettersRouter = require('./routes/lettersRouter');
const chordsRouter = require('./routes/chordsRouter');

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors({ origin: '*' }));
app.use(bodyParser.json());

db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.get('/', (req, res) => {
    res.send('Hello World!');
});

app.use('/api', artistRouter);
app.use('/api', userRouter);
app.use('/api', lastAddedSongsRouter);
app.use('/api', lettersRouter);
app.use('/api', chordsRouter);

module.exports = app;
