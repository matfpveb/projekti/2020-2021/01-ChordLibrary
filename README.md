![react](https://badges.aleen42.com/src/react.svg)
![node](https://badges.aleen42.com/src/node.svg)

# Project ChordLibrary


ChordLibrary je veb aplikacija inspirisana projektom Pesmarica(http://pesmarica.rs/). U odnosu na originalnu inspiraciju biće modernijeg UI dizajna, dok će implementirati većinu funkcionalnosti na koje su naviknuti korisnici sličnih sistema. ChordLibrary je namenjen muzičarima kao sistem za deljenje akorda pesama na jednom mestu. Postojaće 3 tipa korisnika: gost, registrovani korisnik i admin. Registrovani korisnici mogu da naprave novu pesmu tako što će popuniti formu i uneti tekst pesme sa akordima. Pesme se čuvaju u bazi i svaki tip korisnika može da pristupi dodatim pesmama. One se prikazuju kroz frontend, grupisano i sortirano po izvodjačima. Za UI/UX dizajn biće korišćen alat Figma, dok će za implementaciju biti korišćen MERN stack.

## 🖥️ Demo
Demo video is available [here](https://youtu.be/rHBVfJUQj_g).

Weekly reports(in Serbian) are available [here](https://gitlab.com/matfpveb/projekti/2020-2021/01-ChordLibrary/-/wikis/Izve%C5%A1taji).

## Prerequisites

- [Make](https://www.gnu.org/software/make/)
- [Docker](https://docs.docker.com/get-docker/)

## 🔨 Building

```sh
git clone https://gitlab.com/matfpveb/projekti/2020-2021/01-ChordLibrary
cd 01-ChordLibrary
sudo make -i run-dev
```

If you have any troubles, feel free to contact us.


## Developers

- [Filip Filipović, 113/2017](https://gitlab.com/ffilipovicc98)
- [Teodora Isailović, 240/2017](https://gitlab.com/teodora_isailovic)
- [Mihailo Vlajković, 247/2017](https://gitlab.com/hamagrid)
- [Boris Cvitak, 161/2017](https://gitlab.com/cb___)
