export const TOGGLE_REGISTER_MODAL = 'toggleRegisterModal';
export const REGISTER_MODAL_USERNAME_CHANGED = 'registerModalUsernameChanged';
export const REGISTER_MODAL_EMAIL_CHANGED = 'registerModalEmailChanged';
export const REGISTER_MODAL_PASSWORD_CHANGED = 'registerModalPasswordChanged';
export const REGISTER_MODAL_RESET_FORM = 'registerModalResetForm';
