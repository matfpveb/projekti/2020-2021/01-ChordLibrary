import {
    TOGGLE_REGISTER_MODAL,
    REGISTER_MODAL_USERNAME_CHANGED,
    REGISTER_MODAL_EMAIL_CHANGED,
    REGISTER_MODAL_PASSWORD_CHANGED,
    REGISTER_MODAL_RESET_FORM,
} from './actionTypes';

const initState = {
    isOpen: false,
    userName: '',
    email: '',
    password: '',
};

const reducer = (state = initState, action) => {
    switch (action.type) {
        case TOGGLE_REGISTER_MODAL:
            return {
                isOpen: !state.isOpen,
            };
        case REGISTER_MODAL_USERNAME_CHANGED:
            return {
                ...state,
                userName: action.payload.value,
            };
        case REGISTER_MODAL_EMAIL_CHANGED:
            return {
                ...state,
                email: action.payload.value,
            };
        case REGISTER_MODAL_PASSWORD_CHANGED:
            return {
                ...state,
                password: action.payload.value,
            };
        case REGISTER_MODAL_RESET_FORM:
            return {
                ...initState,
                isOpen: state.isOpen,
            };
        default:
            return state;
    }
};

export default reducer;
