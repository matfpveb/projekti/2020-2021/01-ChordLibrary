import {
    TOGGLE_REGISTER_MODAL,
    REGISTER_MODAL_USERNAME_CHANGED,
    REGISTER_MODAL_EMAIL_CHANGED,
    REGISTER_MODAL_PASSWORD_CHANGED,
    REGISTER_MODAL_RESET_FORM,
} from './actionTypes';

export const toggleRegisterModal = () => {
    return {
        type: TOGGLE_REGISTER_MODAL,
    };
};

export const registerModalUsernameChanged = (value) => {
    return {
        type: REGISTER_MODAL_USERNAME_CHANGED,
        payload: { value },
    };
};

export const registerModalEmailChanged = (value) => {
    return {
        type: REGISTER_MODAL_EMAIL_CHANGED,
        payload: { value },
    };
};

export const registerModalPasswordChanged = (value) => {
    return {
        type: REGISTER_MODAL_PASSWORD_CHANGED,
        payload: { value },
    };
};

export const registerModalResetForm = () => {
    return {
        type: REGISTER_MODAL_RESET_FORM,
        payload: {},
    };
};
