import { WINDOW_RESIZE } from './actionTypes';

const initState = {
    width: window.innerWidth,
    // heigth: window.innerHeight,
    type: window.innerWidth > 1360 + 2 * 32 ? 'Desktop' : 'Mobile',
    numberOfResizes: 0,
};

const reducer = (state = initState, action) => {
    switch (action.type) {
        case WINDOW_RESIZE:
            return {
                width: window.innerWidth,
                // heigth: window.innerHeight,
                type: window.innerWidth > 1360 + 2 * 32 ? 'Desktop' : 'Mobile',
                numberOfResizes: state.numberOfResizes + 1,
            };
        default:
            return state;
    }
};

export default reducer;
