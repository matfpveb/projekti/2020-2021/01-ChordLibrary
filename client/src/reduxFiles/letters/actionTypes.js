export const FETCH_LETTERS_REQUEST = 'fetchLettersRequest';
export const FETCH_LETTERS_SUCCESS = 'fetchLettersSuccess';
export const FETCH_LETTERS_ERROR = 'fetchLettersError';
