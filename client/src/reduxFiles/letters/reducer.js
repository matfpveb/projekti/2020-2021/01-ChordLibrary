import {
    FETCH_LETTERS_REQUEST,
    FETCH_LETTERS_SUCCESS,
    FETCH_LETTERS_ERROR,
} from './actionTypes';

const initState = {
    loading: false,
    letters: [],
    error: '',
};

const reducer = (state = initState, action) => {
    switch (action.type) {
        case FETCH_LETTERS_REQUEST:
            return {
                loading: true,
                letters: [],
                error: '',
            };
        case FETCH_LETTERS_SUCCESS:
            return {
                loading: false,
                letters: action.payload.letters,
                error: '',
            };
        case FETCH_LETTERS_ERROR:
            return {
                loading: false,
                letters: [],
                error: action.payload.errorMessage,
            };
        default:
            return state;
    }
};

export default reducer;
