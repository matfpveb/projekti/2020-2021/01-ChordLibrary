import {
    FETCH_LETTERS_REQUEST,
    FETCH_LETTERS_SUCCESS,
    FETCH_LETTERS_ERROR,
} from './actionTypes';

const fetchLettersRequest = () => {
    return {
        type: FETCH_LETTERS_REQUEST,
        payload: {},
    };
};

const fetchLettersSuccess = (letters) => {
    return {
        type: FETCH_LETTERS_SUCCESS,
        payload: { letters },
    };
};

const fetchLettersError = (errorMessage) => {
    return {
        type: FETCH_LETTERS_ERROR,
        payload: { errorMessage },
    };
};

export const fetchLetters = () => {
    return async (dispatch, getState) => {
        try {
            dispatch(fetchLettersRequest());
            const response = await fetch('http://localhost:7000/api/letters');
            const json = await response.json();
            const letters = json;
            dispatch(fetchLettersSuccess(letters));
        } catch (error) {
            dispatch(fetchLettersError(error.message));
        }
    };
};
