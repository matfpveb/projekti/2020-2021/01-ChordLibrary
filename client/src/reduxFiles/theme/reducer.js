import { CHANGE_THEME } from './actionTypes';

const themes = {
    dark: {
        mainBg1: '#2A3784',
        mainBg2: '#2A3784',
        mainBg3: '#262F67',
        textWhite: '#FFFFFF',
        purple1: '#8D6CE2',
        purple2: 'rgba(114, 61, 255, 1)',
        purple3: '#9039FF',
        purpleGradientActive:
            'linear-gradient(90deg, rgba(114, 61, 255, 1)0%, rgba(153, 117, 250, 1)100%)',
        purpleGradientActive2:
            'linear-gradient(90deg, rgba(114, 61, 255, 0.75)0%, rgba(153, 117, 250, 0.85) 100%)',
        purpleGradientInactive:
            'linear-gradient(90deg, rgba(114, 61, 255, 0.05) 0%, rgba(153, 117, 250, 0.05) 100%), rgba(24, 23, 61, 0.5)',
        purpleGradientInactive2:
            'linear-gradient(90deg, rgba(114, 61, 255, 0.2) 0%, rgba(153, 117, 250, 0.2) 100%)',
        widgetBlue1: '#18173D',
        widgetBlue2: '#131F48',
        red: '#FF6060',
        green: '#28A745',
        settingsWidgetDark: 'rgba(255, 255, 255, 0.05)',
        settingsWidgetLight: 'rgba(255, 255, 255, 0.12)',
        menuSettingsItemSmallText: ' rgba(255, 255, 255, 0.7)',
    },
    light: {},
};

themes.light = themes.dark;

const initState = {
    isDarkThemeSelected: true,
    currentTheme: themes.dark,
};

const reducer = (state = initState, action) => {
    switch (action.type) {
        case CHANGE_THEME:
            if (state.isDarkThemeSelected) {
                return {
                    isDarkThemeSelected: false,
                    currentTheme: themes.light,
                };
            }
            return {
                isDarkThemeSelected: true,
                currentTheme: themes.dark,
            };
        default:
            return state;
    }
};

export default reducer;
