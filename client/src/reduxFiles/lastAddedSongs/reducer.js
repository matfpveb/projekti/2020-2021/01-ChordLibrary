import {
    FETCH_LAST_ADDED_SONGS_REQUEST,
    FETCH_LAST_ADDED_SONGS_SUCCESS,
    FETCH_LAST_ADDED_SONGS_ERROR,
} from './actionTypes';

const initState = {
    loading: false,
    songs: [],
    error: '',
};

const reducer = (state = initState, action) => {
    switch (action.type) {
        case FETCH_LAST_ADDED_SONGS_REQUEST:
            return {
                loading: true,
                songs: [],
                error: '',
            };
        case FETCH_LAST_ADDED_SONGS_SUCCESS:
            return {
                loading: false,
                songs: action.payload.lastAddedSongs,
                error: '',
            };
        case FETCH_LAST_ADDED_SONGS_ERROR:
            return {
                loading: false,
                songs: [],
                error: action.payload.errorMessage,
            };
        default:
            return state;
    }
};

export default reducer;
