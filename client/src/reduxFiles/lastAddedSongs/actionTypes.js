export const FETCH_LAST_ADDED_SONGS_REQUEST = 'fetchLastAddedSongsRequest';
export const FETCH_LAST_ADDED_SONGS_SUCCESS = 'fetchLastAddedSongsSuccess';
export const FETCH_LAST_ADDED_SONGS_ERROR = 'fetchLastAddedSongsError';
