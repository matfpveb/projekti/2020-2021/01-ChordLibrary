import {
    FETCH_LAST_ADDED_SONGS_REQUEST,
    FETCH_LAST_ADDED_SONGS_SUCCESS,
    FETCH_LAST_ADDED_SONGS_ERROR,
} from './actionTypes';

const fetchLastAddedSongsRequest = () => {
    return {
        type: FETCH_LAST_ADDED_SONGS_REQUEST,
        payload: {},
    };
};

const fetchLastAddedSongsSuccess = (lastAddedSongs) => {
    return {
        type: FETCH_LAST_ADDED_SONGS_SUCCESS,
        payload: { lastAddedSongs },
    };
};

const fetchLastAddedSongsError = (errorMessage) => {
    return {
        type: FETCH_LAST_ADDED_SONGS_ERROR,
        payload: { errorMessage },
    };
};

export const fetchLastAddedSongs = () => {
    return async (dispatch, getState) => {
        try {
            dispatch(fetchLastAddedSongsRequest());
            const response = await fetch(
                'http://localhost:7000/api/lastAddedSongs'
            );
            const json = await response.json();
            const lastAddedSongs = json;
            dispatch(fetchLastAddedSongsSuccess(lastAddedSongs));
        } catch (error) {
            dispatch(fetchLastAddedSongsError(error.message));
        }
    };
};
