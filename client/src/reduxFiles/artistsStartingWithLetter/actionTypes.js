export const FETCH_ARTISTS_STARTING_WITH_LETTER_REQUEST =
    'fetchArtistsStartingWithLetterRequest';
export const FETCH_ARTISTS_STARTING_WITH_LETTER_SUCCESS =
    'fetchArtistsStartingWithLetterSuccess';
export const FETCH_ARTISTS_STARTING_WITH_LETTER_ERROR =
    'fetchArtistsStartingWithLetterError';
