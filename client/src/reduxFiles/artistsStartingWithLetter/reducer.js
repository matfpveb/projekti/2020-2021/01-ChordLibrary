import {
    FETCH_ARTISTS_STARTING_WITH_LETTER_REQUEST,
    FETCH_ARTISTS_STARTING_WITH_LETTER_SUCCESS,
    FETCH_ARTISTS_STARTING_WITH_LETTER_ERROR,
} from './actionTypes';

const initState = {
    loading: false,
    artists: [],
    error: '',
};

const reducer = (state = initState, action) => {
    switch (action.type) {
        case FETCH_ARTISTS_STARTING_WITH_LETTER_REQUEST:
            return {
                loading: true,
                artists: [],
                error: '',
            };
        case FETCH_ARTISTS_STARTING_WITH_LETTER_SUCCESS:
            return {
                loading: false,
                artists: action.payload.artists,
                error: '',
            };
        case FETCH_ARTISTS_STARTING_WITH_LETTER_ERROR:
            return {
                loading: false,
                artists: [],
                error: action.payload.errorMessage,
            };
        default:
            return state;
    }
};

export default reducer;
