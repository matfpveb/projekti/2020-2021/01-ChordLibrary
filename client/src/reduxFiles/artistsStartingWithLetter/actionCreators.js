import {
    FETCH_ARTISTS_STARTING_WITH_LETTER_REQUEST,
    FETCH_ARTISTS_STARTING_WITH_LETTER_SUCCESS,
    FETCH_ARTISTS_STARTING_WITH_LETTER_ERROR,
} from './actionTypes';

const fetchArtistsStartingWithLetterRequest = () => {
    return {
        type: FETCH_ARTISTS_STARTING_WITH_LETTER_REQUEST,
        payload: {},
    };
};

const fetchArtistsStartingWithLetterSuccess = (artists) => {
    return {
        type: FETCH_ARTISTS_STARTING_WITH_LETTER_SUCCESS,
        payload: { artists },
    };
};

const fetchArtistsStartingWithLetterError = (errorMessage) => {
    return {
        type: FETCH_ARTISTS_STARTING_WITH_LETTER_ERROR,
        payload: { errorMessage },
    };
};

export const fetchArtistsStartingWithLetter = (letter) => {
    return async (dispatch, getState) => {
        try {
            dispatch(fetchArtistsStartingWithLetterRequest());
            const response = await fetch(
                `http://localhost:7000/api/artists/${letter}`
            );
            const json = await response.json();
            const artists = json;
            dispatch(fetchArtistsStartingWithLetterSuccess(artists));
        } catch (error) {
            dispatch(fetchArtistsStartingWithLetterError(error.message));
        }
    };
};
