import {
    ADD_SONG_FORM_ARTIST_NAME_CHANGED,
    ADD_SONG_FORM_SONG_NAME_CHANGED,
    ADD_SONG_FORM_SONG_CONTENT_CHANGED,
    ADD_SONG_FORM_RESET,
} from './actionTypes';

export const addSongFormArtistNameChanged = (value) => {
    return {
        type: ADD_SONG_FORM_ARTIST_NAME_CHANGED,
        payload: { value },
    };
};

export const addSongFormSongNameChanged = (value) => {
    return {
        type: ADD_SONG_FORM_SONG_NAME_CHANGED,
        payload: { value },
    };
};

export const addSongFormSongContentChanged = (value) => {
    return {
        type: ADD_SONG_FORM_SONG_CONTENT_CHANGED,
        payload: { value },
    };
};

export const addSongFormReset = () => {
    return {
        type: ADD_SONG_FORM_RESET,
        payload: {},
    };
};
