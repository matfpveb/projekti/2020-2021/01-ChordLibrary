import {
    ADD_SONG_FORM_ARTIST_NAME_CHANGED,
    ADD_SONG_FORM_SONG_NAME_CHANGED,
    ADD_SONG_FORM_SONG_CONTENT_CHANGED,
    ADD_SONG_FORM_RESET,
} from './actionTypes';

const initState = {
    artistName: '',
    songName: '',
    songContent: '',
};

const reducer = (state = initState, action) => {
    switch (action.type) {
        case ADD_SONG_FORM_ARTIST_NAME_CHANGED:
            return {
                ...state,
                artistName: action.payload.value,
            };
        case ADD_SONG_FORM_SONG_NAME_CHANGED:
            return {
                ...state,
                songName: action.payload.value,
            };
        case ADD_SONG_FORM_SONG_CONTENT_CHANGED:
            return {
                ...state,
                songContent: action.payload.value,
            };
        case ADD_SONG_FORM_RESET:
            return initState;
        default:
            return state;
    }
};

export default reducer;
