export const ADD_SONG_FORM_ARTIST_NAME_CHANGED = 'addSongFormArtistNameChanged';
export const ADD_SONG_FORM_SONG_NAME_CHANGED = 'addSongFormSongNameChanged';
export const ADD_SONG_FORM_SONG_CONTENT_CHANGED =
    'addSongFormSongContentChanged';
export const ADD_SONG_FORM_RESET = 'addSongFormReset';
