import { toggleLoginModal } from '../loginModal/actionCreators';
import { toggleRegisterModal } from '../registerModal/actionCreators';
import {
    FETCH_USER_REQUEST,
    FETCH_USER_SUCCESS,
    AUTH_ERROR,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    LOGOUT_SUCCESS,
    REGISTER_SUCCESS,
    REGISTER_FAIL,
    GET_ERROR,
    CLEAR_ERRORS,
} from './actionTypes';

export const fetchUserRequest = () => {
    return {
        type: FETCH_USER_REQUEST,
        payload: {},
    };
};

export const fetchUserSuccess = (user) => {
    return {
        type: FETCH_USER_SUCCESS,
        payload: { user },
    };
};

export const authError = () => {
    return {
        type: AUTH_ERROR,
        payload: {},
    };
};

export const loginSuccess = ({ token, user }) => {
    return {
        type: LOGIN_SUCCESS,
        payload: { token, user },
    };
};

export const loginFail = () => {
    return {
        type: LOGIN_FAIL,
        payload: {},
    };
};

export const logoutSuccess = () => {
    return {
        type: LOGOUT_SUCCESS,
        payload: {},
    };
};

export const registerSuccess = ({ token, user }) => {
    return {
        type: REGISTER_SUCCESS,
        payload: { token, user },
    };
};

export const registerFail = () => {
    return {
        type: REGISTER_FAIL,
        payload: {},
    };
};

export const getError = (msg, status, id = null) => {
    return {
        type: GET_ERROR,
        payload: {
            error: {
                msg,
                status,
                id,
            },
        },
    };
};

export const clearErrors = () => {
    return {
        type: CLEAR_ERRORS,
        payload: {},
    };
};

export const loadUser = () => {
    return async (dispatch, getState) => {
        try {
            dispatch(fetchUserRequest());

            const response = await fetch(
                'http://localhost:7000/api/user/loaduser',
                tokenConfig(getState().user.token)
            );

            const json = await response.json();
            if (response.status !== 200) {
                throw json;
            }
            const user = json;
            dispatch(fetchUserSuccess(user));
        } catch (error) {
            console.log('Greska:', error);
            dispatch(getError(error?.response?.data, error?.response?.status));
            dispatch(authError());
        }
    };
};

export const register = (name, password) => {
    return async (dispatch, getState) => {
        try {
            const config = {
                method: 'POST',
                headers: {
                    'Content-type': 'application/json',
                },
                body: JSON.stringify({ username: name, password }),
            };

            const response = await fetch(
                'http://localhost:7000/api/user/register',
                config
            );
            const json = await response.json();
            const userWithToken = json;
            dispatch(toggleRegisterModal());
            dispatch(registerSuccess(userWithToken));
        } catch (error) {
            dispatch(
                getError(
                    error?.response?.data,
                    error?.response?.status,
                    'REGISTER_FAIL'
                )
            );
            dispatch(registerFail());
        }
    };
};

export const login = (userName, password) => {
    return async (dispatch, getState) => {
        try {
            const config = {
                method: 'POST',
                headers: {
                    'Content-type': 'application/json',
                },
                body: JSON.stringify({ username: userName, password }),
            };

            const response = await fetch(
                'http://localhost:7000/api/user/login',
                config
            ); // ???
            const json = await response.json();
            const userWithToken = json;
            dispatch(toggleLoginModal());
            dispatch(loginSuccess(userWithToken));
        } catch (error) {
            console.log(error);
            dispatch(
                getError(
                    error?.response?.data,
                    error?.response?.status,
                    'LOGIN_FAIL'
                )
            );
            dispatch(loginFail());
        }
    };
};

export const logout = () => {
    return logoutSuccess();
};

const tokenConfig = (token) => {
    const config = {
        headers: {
            'Content-type': 'aplication / json',
        },
    };

    if (token) {
        config.headers['x-auth-token'] = token;
    }

    return config;
};
