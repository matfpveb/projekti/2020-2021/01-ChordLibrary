import {
    FETCH_USER_REQUEST,
    FETCH_USER_SUCCESS,
    AUTH_ERROR,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    LOGOUT_SUCCESS,
    REGISTER_SUCCESS,
    REGISTER_FAIL,
    GET_ERROR,
    CLEAR_ERRORS,
} from './actionTypes';

const initState = {
    loading: false,
    token: localStorage.getItem('token'), // null if token doesn't exist in localStorage
    isAuthenticated: false,
    user: null,
    error: { msg: {}, status: null, id: null },
};

const reducer = (state = initState, action) => {
    switch (action.type) {
        case FETCH_USER_REQUEST:
            return {
                ...state,
                loading: true,
            };
        case FETCH_USER_SUCCESS:
            return {
                ...state,
                isAuthenticated: true,
                loading: false,
                user: action.payload.user,
            };
        case LOGIN_SUCCESS:
        case REGISTER_SUCCESS:
            localStorage.setItem('token', action.payload.token);
            return {
                ...state,
                token: action.payload.token,
                user: action.payload.user,
                isAuthenticated: true,
                loading: false,
            };
        case AUTH_ERROR:
        case LOGIN_FAIL:
        case LOGOUT_SUCCESS:
        case REGISTER_FAIL:
            localStorage.removeItem('token');
            return {
                ...state,
                ...initState,
                token: null,
            };
        case GET_ERROR:
            return {
                ...state,
                error: {
                    msg: action.payload.error.msg,
                    status: action.payload.error.status,
                    id: action.payload.error.id,
                },
            };
        case CLEAR_ERRORS:
            return {
                ...state,
                error: initState.error,
            };
        default:
            return state;
    }
};

export default reducer;
