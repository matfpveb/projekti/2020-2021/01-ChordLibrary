export const FETCH_CHORDS_REQUEST = 'fetchChordsRequest';
export const FETCH_CHORDS_SUCCESS = 'fetchChordsSuccess';
export const FETCH_CHORDS_ERROR = 'fetchChordsError';
