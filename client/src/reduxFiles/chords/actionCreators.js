import { fetchArtist } from '../artist/actionCreators';
import {
    FETCH_CHORDS_REQUEST,
    FETCH_CHORDS_SUCCESS,
    FETCH_CHORDS_ERROR,
} from './actionTypes';

const fetchChordsRequest = () => {
    return {
        type: FETCH_CHORDS_REQUEST,
        payload: {},
    };
};

const fetchChordsSuccess = (chords) => {
    return {
        type: FETCH_CHORDS_SUCCESS,
        payload: chords,
    };
};

const fetchChordsError = (errorMessage) => {
    return {
        type: FETCH_CHORDS_ERROR,
        payload: { errorMessage },
    };
};

export const fetchChords = (songID) => {
    return async (dispatch, getState) => {
        try {
            dispatch(fetchChordsRequest());
            const response = await fetch(
                `http://localhost:7000/api/chords/${songID}`
            );
            const json = await response.json();
            const chords = json;
            dispatch(fetchChordsSuccess(chords));
            dispatch(fetchArtist(chords.artistID));
        } catch (error) {
            dispatch(fetchChordsError(error.message));
        }
    };
};
