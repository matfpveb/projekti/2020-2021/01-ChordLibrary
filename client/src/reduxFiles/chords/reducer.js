import {
    FETCH_CHORDS_REQUEST,
    FETCH_CHORDS_SUCCESS,
    FETCH_CHORDS_ERROR,
} from './actionTypes';

const initState = {
    loading: false,
    artistName: '',
    artistID: -1,
    songName: '',
    songID: -1,
    content: '',
    error: '',
};

const reducer = (state = initState, action) => {
    switch (action.type) {
        case FETCH_CHORDS_REQUEST:
            return {
                loading: true,
                artistName: '',
                artistID: -1,
                songName: '',
                songID: -1,
                content: '',
                error: '',
            };
        case FETCH_CHORDS_SUCCESS:
            return {
                loading: false,
                artistName: action.payload.artistName,
                artistID: action.payload.artistID,
                songName: action.payload.songName,
                songID: action.payload.songID,
                content: JSON.parse(action.payload.content),
                error: '',
            };
        case FETCH_CHORDS_ERROR:
            return {
                loading: false,
                artistName: '',
                artistID: -1,
                songName: '',
                songID: -1,
                content: '',
                error: action.payload.errorMessage,
            };
        default:
            return state;
    }
};

export default reducer;
