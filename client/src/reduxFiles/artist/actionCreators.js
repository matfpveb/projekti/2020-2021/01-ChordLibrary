import {
    FETCH_ARTIST_REQUEST,
    FETCH_ARTIST_SUCCESS,
    FETCH_ARTIST_ERROR,
} from './actionTypes';

const fetchArtistRequest = () => {
    return {
        type: FETCH_ARTIST_REQUEST,
        payload: {},
    };
};

const fetchArtistSuccess = (artist) => {
    return {
        type: FETCH_ARTIST_SUCCESS,
        payload: artist,
    };
};

const fetchArtistError = (errorMessage) => {
    return {
        type: FETCH_ARTIST_ERROR,
        payload: { errorMessage },
    };
};

export const fetchArtist = (artistID) => {
    return async (dispatch, getState) => {
        try {
            dispatch(fetchArtistRequest());
            const response = await fetch(
                `http://localhost:7000/api/artist/${artistID}`
            );
            const json = await response.json();
            const artist = json;
            dispatch(fetchArtistSuccess(artist));
        } catch (error) {
            dispatch(fetchArtistError(error.message));
        }
    };
};
