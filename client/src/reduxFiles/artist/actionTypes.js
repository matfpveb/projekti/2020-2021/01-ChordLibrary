export const FETCH_ARTIST_REQUEST = 'fetchArtistRequest';
export const FETCH_ARTIST_SUCCESS = 'fetchArtistSuccess';
export const FETCH_ARTIST_ERROR = 'fetchArtistError';
