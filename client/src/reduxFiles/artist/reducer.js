import {
    FETCH_ARTIST_REQUEST,
    FETCH_ARTIST_SUCCESS,
    FETCH_ARTIST_ERROR,
} from './actionTypes';

const initState = {
    loading: false,
    artistName: 'Artist name',
    artistID: -1,
    songs: [],
    numberOfSongs: 0,
    about: '',
    error: '',
};

const reducer = (state = initState, action) => {
    switch (action.type) {
        case FETCH_ARTIST_REQUEST:
            return {
                loading: true,
                artistName: 'Artist name',
                artistID: -1,
                songs: [],
                numberOfSongs: 0,
                about: '',
                error: '',
            };
        case FETCH_ARTIST_SUCCESS:
            return {
                loading: false,
                artistName: action.payload.artistName,
                artistID: action.payload.artistID,
                songs: action.payload.songs,
                numberOfSongs: action.payload.numberOfSongs,
                about: action.payload.about,
                error: '',
            };
        case FETCH_ARTIST_ERROR:
            return {
                loading: false,
                artistName: 'Artist name',
                artistID: -1,
                songs: [],
                numberOfSongs: 0,
                about: '',
                error: action.payload.errorMessage,
            };
        default:
            return state;
    }
};

export default reducer;
