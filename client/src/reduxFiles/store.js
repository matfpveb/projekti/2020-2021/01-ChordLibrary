import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import lastAddedSongsReducer from './lastAddedSongs/reducer';
import themeReducer from './theme/reducer';
import deviceReducer from './device/reducer';
import lettersReducer from './letters/reducer';
import artistsStartingWithLetterReducer from './artistsStartingWithLetter/reducer';
import artistReducer from './artist/reducer';
import chordsReducer from './chords/reducer';
import userReducer from './user/reducer';
import loginModalReducer from './loginModal/reducer';
import registerModalReducer from './registerModal/reducer';
import userCardDropDownReducer from './userCardDropDown/reducer';
import addSongFormReducer from './addSongForm/reducer';
import thunk from 'redux-thunk';

const rootReducer = combineReducers({
    lastAddedSongs: lastAddedSongsReducer,
    themes: themeReducer,
    device: deviceReducer,
    letters: lettersReducer,
    artists: artistsStartingWithLetterReducer,
    artist: artistReducer,
    currentSong: chordsReducer,
    user: userReducer,
    loginModal: loginModalReducer,
    registerModal: registerModalReducer,
    userCardDropDown: userCardDropDownReducer,
    addSongForm: addSongFormReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; // DEBUG

const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(thunk))
);

export default store;
