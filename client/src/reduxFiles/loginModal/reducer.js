import {
    TOGGLE_LOGIN_MODAL,
    LOGIN_MODAL_USERNAME_CHANGED,
    LOGIN_MODAL_PASSWORD_CHANGED,
    LOGIN_MODAL_RESET_FORM,
} from './actionTypes';

const initState = {
    isOpen: false,
    userName: '',
    password: '',
};

const reducer = (state = initState, action) => {
    switch (action.type) {
        case TOGGLE_LOGIN_MODAL:
            return {
                ...initState,
                isOpen: !state.isOpen,
            };
        case LOGIN_MODAL_USERNAME_CHANGED:
            return {
                ...state,
                userName: action.payload.value,
            };
        case LOGIN_MODAL_PASSWORD_CHANGED:
            return {
                ...state,
                password: action.payload.value,
            };
        case LOGIN_MODAL_RESET_FORM:
            return {
                ...initState,
                isOpen: state.isOpen,
            };
        default:
            return state;
    }
};

export default reducer;
