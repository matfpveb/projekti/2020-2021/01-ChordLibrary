import {
    TOGGLE_LOGIN_MODAL,
    LOGIN_MODAL_USERNAME_CHANGED,
    LOGIN_MODAL_PASSWORD_CHANGED,
    LOGIN_MODAL_RESET_FORM,
} from './actionTypes';

export const toggleLoginModal = () => {
    return {
        type: TOGGLE_LOGIN_MODAL,
    };
};

export const loginModalUserNameChanged = (value) => {
    return {
        type: LOGIN_MODAL_USERNAME_CHANGED,
        payload: { value },
    };
};

export const loginModalPasswordChanged = (value) => {
    return {
        type: LOGIN_MODAL_PASSWORD_CHANGED,
        payload: { value },
    };
};

export const loginModalResetForm = () => {
    return {
        type: LOGIN_MODAL_RESET_FORM,
        payload: {},
    };
};
