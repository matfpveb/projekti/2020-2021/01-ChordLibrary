export const TOGGLE_LOGIN_MODAL = 'toggleLoginModal';
export const LOGIN_MODAL_USERNAME_CHANGED = 'loginModalUserNameChanged';
export const LOGIN_MODAL_PASSWORD_CHANGED = 'loginModalPasswordChanged';
export const LOGIN_MODAL_RESET_FORM = 'loginModalResetForm';
