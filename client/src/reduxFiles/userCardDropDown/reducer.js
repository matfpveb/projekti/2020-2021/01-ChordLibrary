import {
    USER_CARD_DROPDOWN_OPEN,
    USER_CARD_DROPDOWN_CLOSE,
} from './actionTypes';

const initState = {
    isOpen: false,
};

const reducer = (state = initState, action) => {
    switch (action.type) {
        case USER_CARD_DROPDOWN_OPEN:
            return {
                isOpen: true,
            };
        case USER_CARD_DROPDOWN_CLOSE:
            return {
                isOpen: false,
            };

        default:
            return state;
    }
};

export default reducer;
