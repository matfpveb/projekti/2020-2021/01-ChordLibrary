import {
    USER_CARD_DROPDOWN_OPEN,
    USER_CARD_DROPDOWN_CLOSE,
} from './actionTypes';

export const userCardDropDownOpen = () => {
    return {
        type: USER_CARD_DROPDOWN_OPEN,
    };
};

export const userCardDropDownClose = (value) => {
    return {
        type: USER_CARD_DROPDOWN_CLOSE,
        payload: { value },
    };
};
