import {
    BrowserRouter as Router,
    Redirect,
    Route,
    Switch,
} from 'react-router-dom';

import './fontLoader.css';
import GlobalStyle from './helperFunctions/globalStyle';

import HomePage from './components/Pages/HomePage/HomePage';
import AboutPage from './components/Pages/AboutPage/AboutPage';
import LettersPage from './components/Pages/LettersPage/LettersPage';
import LetterPage from './components/Pages/LetterPage/LetterPage';
import ArtistPage from './components/Pages/ArtistPage/ArtistPage';
import SongChordsPage from './components/Pages/SongChordsPage/SongChordsPage';
import SettingsPage from './components/Pages/SettingsPage/SettingsPage';
import { useDispatch } from 'react-redux';
import { loadUser } from './reduxFiles/user/actionCreators';
import { useEffect } from 'react';
import AddPage from './components/Pages/AddPage/AddPage';
import PrivateRoute from './components/PrivateRoute/PrivateRoute';

const App = () => {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(loadUser());
    }, []); // eslint-disable-line react-hooks/exhaustive-deps
    return (
        <>
            <GlobalStyle />
            <Router>
                <Switch>
                    <PrivateRoute exact path='/add'>
                        <AddPage />
                    </PrivateRoute>
                    <PrivateRoute exact path='/settings'>
                        <SettingsPage />
                    </PrivateRoute>
                    <Route exact path='/chords/:id'>
                        <SongChordsPage />
                    </Route>
                    <Route exact path='/artist/:id'>
                        <ArtistPage />
                    </Route>
                    <Route exact path='/letter/:letter'>
                        <LetterPage />
                    </Route>
                    <Route exact path='/letters'>
                        <LettersPage />
                    </Route>
                    <Route exact path='/about'>
                        <AboutPage />
                    </Route>
                    <Route exact path='/'>
                        <HomePage />
                    </Route>
                    <Redirect to='/' />
                </Switch>
            </Router>
        </>
    );
};

export default App;
