import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`

    *,
    *::after,
    *::before {
        box-sizing: border-box;
        margin: 0;
        padding: 0;
        color: inherit;
    }
    html {
        box-sizing: border-box;
        font-family: 'Montserrat', sans-serif;
    }
    body {
        min-height: 100vh;
        height: 100vh;
        /* overflow: hidden; */
    }
    #root {
        height: 100%;
    }
    a {
        text-decoration: none;
    }
    
`;

export default GlobalStyle;
