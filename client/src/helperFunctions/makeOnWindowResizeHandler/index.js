import { throttle } from 'lodash';

import { windowResize } from '../../reduxFiles/device/actionCreators';

const makeOnWindowResizeHandler = (dispatch, device, throttleTimeInMS) => {
    return throttle(
        (() => {
            let lastWidth = device.width;
            return () => {
                if (lastWidth === window.innerWidth) {
                    return;
                }
                lastWidth = window.innerWidth;
                dispatch(windowResize());
            };
        })(),
        throttleTimeInMS
    );
};

export default makeOnWindowResizeHandler;
