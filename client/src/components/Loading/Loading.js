import React from 'react';
import Loader from 'react-loader-spinner';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

import ResponsivePSmall1 from '../TextStyles/Responsive/ResponsivePSmall1';

const Loading = () => {
    const currentTheme = useSelector((state) => state.themes.currentTheme);
    return (
        <Wrapper>
            <Box background={currentTheme.settingsWidgetDark}>
                <ResponsivePSmall1>Loading...</ResponsivePSmall1>
                <Loader
                    type='Oval'
                    color={`${currentTheme.purple3}`}
                    height={50}
                    width={50}
                />
            </Box>
        </Wrapper>
    );
};

const Box = styled.div`
    background: ${(props) => props.background};

    width: 150px;
    height: 150px;
    border-radius: 16px;

    display: grid;
    justify-items: center;
    align-items: center;
    align-content: center;
    justify-content: center;
    gap: 16px;
`;

const Wrapper = styled.div`
    width: 100%;

    display: grid;
    justify-items: center;
`;

export default Loading;
