import React from 'react';

import { RiFolderMusicLine as Icon } from 'react-icons/ri';

import IconWrapper from './IconWrapper';

const MusicFolderOutlineIcon = () => {
    return (
        <IconWrapper>
            <Icon size='24px' />
        </IconWrapper>
    );
};

export default MusicFolderOutlineIcon;
