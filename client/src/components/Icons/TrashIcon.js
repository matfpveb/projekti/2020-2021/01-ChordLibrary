import React from 'react';

import { VscTrash as Icon } from 'react-icons/vsc';

import IconWrapper from './IconWrapper';

const TrashIcon = () => {
    return (
        <IconWrapper>
            <Icon size='24px' />
        </IconWrapper>
    );
};

export default TrashIcon;
