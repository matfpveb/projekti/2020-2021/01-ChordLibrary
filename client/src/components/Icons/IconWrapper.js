import React from 'react';
import styled from 'styled-components';

const IconWrapper = ({ children }) => {
    return <Wrapper>{children}</Wrapper>;
};

const Wrapper = styled.div`
    width: 24px;
    height: 24px;

    display: grid;
    align-items: center;
    justify-items: center;
`;

export default IconWrapper;
