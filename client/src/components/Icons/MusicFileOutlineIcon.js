import React from 'react';

import { RiFileMusicLine as Icon } from 'react-icons/ri';

import IconWrapper from './IconWrapper';

const MusicFileOutlineIcon = () => {
    return (
        <IconWrapper>
            <Icon size='24px' />
        </IconWrapper>
    );
};

export default MusicFileOutlineIcon;
