import React from 'react';

import { IoMusicalNotesOutline as Icon } from 'react-icons/io5';

import IconWrapper from './IconWrapper';

const MusicNoteIcon = () => {
    return (
        <IconWrapper>
            <Icon size='24px' />
        </IconWrapper>
    );
};

export default MusicNoteIcon;
