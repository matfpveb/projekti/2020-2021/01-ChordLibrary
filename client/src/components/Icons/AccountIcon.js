import React from 'react';

import { VscAccount as Icon } from 'react-icons/vsc';

import IconWrapper from './IconWrapper';

const AccountIcon = () => {
    return (
        <IconWrapper>
            <Icon size='24px' />
        </IconWrapper>
    );
};

export default AccountIcon;
