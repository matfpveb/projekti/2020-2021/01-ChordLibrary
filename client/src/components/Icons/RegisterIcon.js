import React from 'react';

import { FaRegAddressBook as Icon } from 'react-icons/fa';

import IconWrapper from './IconWrapper';

const RegisterIcon = () => {
    return (
        <IconWrapper>
            <Icon size='24px' />
        </IconWrapper>
    );
};

export default RegisterIcon;
