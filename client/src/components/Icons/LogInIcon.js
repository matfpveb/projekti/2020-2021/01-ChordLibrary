import React from 'react';

import { IoLogOutOutline as Icon } from 'react-icons/io5';

import IconWrapper from './IconWrapper';

const LeftArrowIcon = () => {
    return (
        <IconWrapper>
            <Icon size='24px' />
        </IconWrapper>
    );
};

export default LeftArrowIcon;
