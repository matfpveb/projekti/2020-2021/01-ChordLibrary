import React from 'react';

import { AiOutlineHome as Icon } from 'react-icons/ai';

import IconWrapper from './IconWrapper';

const HomeIcon = () => {
    return (
        <IconWrapper>
            <Icon size='24px' />
        </IconWrapper>
    );
};

export default HomeIcon;
