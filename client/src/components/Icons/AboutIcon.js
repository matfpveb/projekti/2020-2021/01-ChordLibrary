import React from 'react';

import { IoInformationCircleOutline as Icon } from 'react-icons/io5';

import IconWrapper from './IconWrapper';

const AboutIcon = () => {
    return (
        <IconWrapper>
            <Icon size='24px' />
        </IconWrapper>
    );
};

export default AboutIcon;
