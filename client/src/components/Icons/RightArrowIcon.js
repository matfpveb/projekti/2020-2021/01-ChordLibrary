import React from 'react';

import { IoIosArrowForward as Icon } from 'react-icons/io';

import IconWrapper from './IconWrapper';

const RightArrowIcon = () => {
    return (
        <IconWrapper>
            <Icon size='24px' />
        </IconWrapper>
    );
};

export default RightArrowIcon;
