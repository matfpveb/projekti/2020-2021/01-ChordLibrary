import React from 'react';

import { AiOutlineSearch as Icon } from 'react-icons/ai';

import IconWrapper from './IconWrapper';

const SearchIcon = () => {
    return (
        <IconWrapper>
            <Icon size='24px' />
        </IconWrapper>
    );
};

export default SearchIcon;
