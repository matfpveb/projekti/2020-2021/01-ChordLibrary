import React from 'react';

import { IoWarningOutline as Icon } from 'react-icons/io5';

import IconWrapper from './IconWrapper';

const WarningIcon = () => {
    return (
        <IconWrapper>
            <Icon size='24px' />
        </IconWrapper>
    );
};

export default WarningIcon;
