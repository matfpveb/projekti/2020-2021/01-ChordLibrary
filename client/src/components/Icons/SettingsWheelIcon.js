import React from 'react';

import { IoSettingsOutline as Icon } from 'react-icons/io5';

import IconWrapper from './IconWrapper';

const SettingsWheelIcon = () => {
    return (
        <IconWrapper>
            <Icon size='24px' />
        </IconWrapper>
    );
};

export default SettingsWheelIcon;
