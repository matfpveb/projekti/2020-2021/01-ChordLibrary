import React from 'react';

import { GiSettingsKnobs as Icon } from 'react-icons/gi';

import IconWrapper from './IconWrapper';

const SettingsSwitchIcon = () => {
    return (
        <IconWrapper>
            <Icon size='24px' />
        </IconWrapper>
    );
};

export default SettingsSwitchIcon;
