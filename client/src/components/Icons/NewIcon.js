import React from 'react';

import { BsFileEarmarkPlus as Icon } from 'react-icons/bs';

import IconWrapper from './IconWrapper';

const NewIcon = () => {
    return (
        <IconWrapper>
            <Icon size='24px' />
        </IconWrapper>
    );
};

export default NewIcon;
