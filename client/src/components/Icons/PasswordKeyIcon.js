import React from 'react';

import { VscKey as Icon } from 'react-icons/vsc';

import IconWrapper from './IconWrapper';

const PasswordKeyIcon = () => {
    return (
        <IconWrapper>
            <Icon size='24px' />
        </IconWrapper>
    );
};

export default PasswordKeyIcon;
