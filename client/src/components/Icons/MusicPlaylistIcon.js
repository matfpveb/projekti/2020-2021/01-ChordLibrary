import React from 'react';

import { BsMusicNoteList as Icon } from 'react-icons/bs';

import IconWrapper from './IconWrapper';

const MusicPlaylistIcon = () => {
    return (
        <IconWrapper>
            <Icon size='24px' />
        </IconWrapper>
    );
};

export default MusicPlaylistIcon;
