import React from 'react';

import { IoLogOutOutline as Icon } from 'react-icons/io5';

import IconWrapper from './IconWrapper';

const LogOutIcon = () => {
    return (
        <IconWrapper>
            <Icon size='24px' style={{ transform: 'rotateZ(180deg)' }} />
        </IconWrapper>
    );
};

export default LogOutIcon;
