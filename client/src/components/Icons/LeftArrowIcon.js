import React from 'react';

import { IoIosArrowForward as Icon } from 'react-icons/io';

import IconWrapper from './IconWrapper';

const LeftArrowIcon = () => {
    return (
        <IconWrapper>
            <Icon size='24px' style={{ transform: 'rotateZ(180deg)' }} />
        </IconWrapper>
    );
};

export default LeftArrowIcon;
