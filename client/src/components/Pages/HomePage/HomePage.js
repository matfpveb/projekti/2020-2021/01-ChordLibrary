import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import { fetchLastAddedSongs } from './../../../reduxFiles/lastAddedSongs/actionCreators';

import PageContainer from './../../PageContainer/PageContainer';
import HomePageIllustration from './HomePageIllustration/HomePageIllustration';
import LastAddedSongs from './LastAddedSongs/LastAddedSongs';

const HomePage = () => {
    const device = useSelector((state) => state.device);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchLastAddedSongs());
        return () => {};
    }, []); // eslint-disable-line react-hooks/exhaustive-deps
    return (
        <PageContainer>
            <Wrapper>
                {device.type === 'Desktop' ? (
                    <>
                        <LastAddedSongs />
                        <HomePageIllustration />
                    </>
                ) : (
                    <>
                        <HomePageIllustration />
                        <LastAddedSongs />
                    </>
                )}
            </Wrapper>
        </PageContainer>
    );
};

const Wrapper = styled.div`
    display: grid;

    grid-template-columns: repeat(12, 84px);
    gap: 0px 32px;

    @media (max-width: calc(1360px + 2 * 32px)) {
        grid-template-columns: repeat(4, 84px);
        gap: 24px 16px;
    }
`;

export default HomePage;
