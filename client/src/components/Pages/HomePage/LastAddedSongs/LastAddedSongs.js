import React from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

import ResponsiveH3 from '../../../TextStyles/Responsive/ResponsiveH3';
import SongCard from './../../../SongCard/SongCard';
import Loading from './../../../Loading/Loading';

const LastAddedSongs = () => {
    const currentTheme = useSelector((state) => state.themes.currentTheme);
    const lastAddedSongs = useSelector((state) => state.lastAddedSongs);

    return (
        <Container background={currentTheme.widgetBlue1}>
            <Header background={currentTheme.widgetBlue2}>
                <ResponsiveH3>Last added songs</ResponsiveH3>
            </Header>
            <CardsList>
                {lastAddedSongs.loading ? (
                    <Loading />
                ) : (
                    lastAddedSongs.songs.map((song) => (
                        <SongCard
                            artistName={song.artistName}
                            songName={song.songName}
                            songID={song.songID}
                            key={song.songID}
                            hasImage={true}
                            transparent='Light'
                        />
                    ))
                )}
            </CardsList>
        </Container>
    );
};

// Styled Components

const Container = styled.div`
    background: ${(props) => props.background};
    grid-column: 1 / span 6;
    align-self: center;
    height: 712px;
    border-radius: 30px;
    overflow: hidden;

    @media (max-width: calc(1360px + 2 * 32px)) {
        grid-column: 1 / span 4;
    }
`;

const Header = styled.div`
    background: ${(props) => props.background};
    height: 120px;
    display: grid;
    align-items: center;
    padding-left: 24px;
`;

const CardsList = styled.div`
    display: grid;
    gap: 16px 0px;
    padding: 24px 16px;
`;

export default LastAddedSongs;
