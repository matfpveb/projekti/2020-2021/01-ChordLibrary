import React from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

import SongNameText from '../../../TextStyles/Responsive/ResponsiveH3';
import ArtistNameAndChordsText from '../../../TextStyles/Responsive/ResponsivePBodyMain';

const ChordsBox = () => {
    // const device = useSelector((state) => state.device);
    const artist = useSelector((state) => state.artist);
    const currentSong = useSelector((state) => state.currentSong);
    const currentTheme = useSelector((state) => state.themes.currentTheme);
    return (
        <Box background={currentTheme.widgetBlue1}>
            <Header background={currentTheme.widgetBlue2}>
                <TextContent>
                    <SongNameText>{currentSong.songName}</SongNameText>
                    <ArtistNameAndChordsText>
                        {artist.artistName}
                    </ArtistNameAndChordsText>
                </TextContent>
            </Header>
            <Chords>{currentSong.content}</Chords>
        </Box>
    );
};

const Box = styled.div`
    background: ${(props) => props.background};
    grid-column: span 8;
    border-radius: 30px;
    overflow: hidden;

    @media (max-width: calc(1360px + 2 * 32px)) {
        grid-column: span 4;
    }
`;

const Header = styled.div`
    background: ${(props) => props.background};
    height: 120px;
    display: grid;
    align-items: center;
    padding-left: 24px;
`;

const TextContent = styled.div``;

const Chords = styled.pre`
    display: grid;
    gap: 16px 0px;
    padding: 24px 16px;
`;

export default ChordsBox;
