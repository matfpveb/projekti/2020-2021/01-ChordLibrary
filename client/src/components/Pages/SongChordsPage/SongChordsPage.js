import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import { fetchChords } from '../../../reduxFiles/chords/actionCreators';

import Loading from '../../Loading/Loading';
import PageContainer from './../../PageContainer/PageContainer';
import ArtistSideBar from '../../ArtistSideBar/ArtistSideBar';
import ChordsBox from './ChordsBox/ChordsBox';

const SongChordsPage = () => {
    let { id } = useParams();
    const device = useSelector((state) => state.device);
    const artist = useSelector((state) => state.artist);
    const currentSong = useSelector((state) => state.currentSong);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchChords(id));
    }, [id]); // eslint-disable-line react-hooks/exhaustive-deps
    return (
        <PageContainer>
            {artist.loading || currentSong.loading ? (
                <Loading />
            ) : (
                <Wrapper>
                    {device.type === 'Desktop' && (
                        <ArtistSideBar isAboutVisible={false} />
                    )}
                    <ChordsBox />
                </Wrapper>
            )}
        </PageContainer>
    );
};

const Wrapper = styled.div`
    display: grid;

    grid-template-columns: repeat(12, 84px);
    gap: 0px 32px;

    @media (max-width: calc(1360px + 2 * 32px)) {
        grid-template-columns: repeat(4, 84px);
        gap: 32px 16px;
    }
`;

export default SongChordsPage;
