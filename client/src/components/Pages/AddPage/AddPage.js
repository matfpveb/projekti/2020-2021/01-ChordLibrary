import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import SaveChangesButton from '../SettingsPage/Buttons/SaveChangesButton';

import PageContainer from './../../PageContainer/PageContainer';

import ResponsiveH3 from './../../TextStyles/Responsive/ResponsiveH3';

import InputWithText from './../SettingsPage/InputWithText/InputWithText';

import {
    addSongFormArtistNameChanged,
    addSongFormSongNameChanged,
    addSongFormSongContentChanged,
    addSongFormReset,
} from './../../../reduxFiles/addSongForm/actionCreators';

const AddPage = () => {
    const currentTheme = useSelector((state) => state.themes.currentTheme);
    const addSongForm = useSelector((state) => state.addSongForm);
    const user = useSelector((state) => state.user);
    const dispatch = useDispatch();
    return (
        <PageContainer>
            <Wrapper>
                <Box background={currentTheme.widgetBlue1}>
                    <Header background={currentTheme.widgetBlue2}>
                        <TextContent>
                            <ResponsiveH3>Add new song</ResponsiveH3>
                        </TextContent>
                    </Header>
                    <AddSongForm>
                        <InputWithText
                            text='Artist name:'
                            onChange={(event) => {
                                dispatch(
                                    addSongFormArtistNameChanged(
                                        event.target.value
                                    )
                                );
                            }}
                            value={addSongForm.artistName}
                        />
                        <InputWithText
                            text='Song name:'
                            onChange={(event) => {
                                dispatch(
                                    addSongFormSongNameChanged(
                                        event.target.value
                                    )
                                );
                            }}
                            value={addSongForm.songName}
                        />
                        <SongTextArea
                            rows='12'
                            cols='50'
                            background={currentTheme.settingsWidgetDark}
                            borderColor={currentTheme.widgetBlue1}
                            textColor={currentTheme.textWhite}
                            spellCheck={false}
                            onChange={(event) => {
                                dispatch(
                                    addSongFormSongContentChanged(
                                        event.target.value
                                    )
                                );
                            }}
                            value={addSongForm.songContent}
                        />
                        <SaveChangesButtonOverride
                            onClickHandler={async (event) => {
                                const config = {
                                    method: 'POST',
                                    headers: {
                                        'Content-type': 'application/json',
                                    },
                                    body: JSON.stringify({
                                        token: user.token,
                                        artistName: addSongForm.artistName,
                                        songName: addSongForm.songName,
                                        content: addSongForm.songContent,
                                    }),
                                };

                                await fetch(
                                    'http://localhost:7000/api/artist',
                                    config
                                );

                                dispatch(addSongFormReset());
                            }}
                        />
                    </AddSongForm>
                </Box>
            </Wrapper>
        </PageContainer>
    );
};

const Wrapper = styled.div`
    display: grid;

    grid-template-columns: repeat(12, 84px);
    gap: 0px 32px;

    @media (max-width: calc(1360px + 2 * 32px)) {
        grid-template-columns: repeat(4, 84px);
        gap: 32px 16px;
    }
`;

const Box = styled.div`
    background: ${(props) => props.background};
    grid-column: 1 / -1;
    border-radius: 30px;
    overflow: hidden;

    @media (max-width: calc(1360px + 2 * 32px)) {
    }
`;

const Header = styled.div`
    background: ${(props) => props.background};
    height: 120px;
    display: grid;
    align-items: center;
    padding-left: 24px;
`;

const TextContent = styled.div``;

const AddSongForm = styled.div`
    display: grid;
    gap: 16px 0px;
    padding: 24px 16px;
`;

const SongTextArea = styled.textarea`
    background: ${(props) => props.background};
    border: 1px solid ${(props) => props.borderColor};
    color: ${(props) => props.textColor};
    width: 100%;
    height: 60vh;
    border-radius: 8px;
    resize: none;
`;

const SaveChangesButtonOverride = styled(SaveChangesButton)`
    justify-self: flex-end;
`;

export default AddPage;
