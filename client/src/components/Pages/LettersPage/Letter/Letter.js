import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import ResponsiveH4Medium from '../../../TextStyles/Responsive/ResponsiveH4Medium';

const Letter = ({ letter }) => {
    const currentTheme = useSelector((state) => state.themes.currentTheme);
    return (
        <Wrapper
            to={`/letter/${letter}`}
            background={currentTheme.purpleGradientActive}
        >
            <ResponsiveH4Medium>{letter}</ResponsiveH4Medium>
        </Wrapper>
    );
};

const Wrapper = styled(Link)`
    background: ${(props) => props.background};

    width: 84px;
    height: 84px;

    display: grid;
    justify-items: center;
    align-items: center;

    border-radius: 16px;
`;

export default Letter;
