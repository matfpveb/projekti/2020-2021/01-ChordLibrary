import React, { useEffect } from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import { fetchLetters } from './../../../reduxFiles/letters/actionCreators';

import PageContainer from './../../PageContainer/PageContainer';
import Letter from './Letter/Letter';
import BoxedH4 from '../../TextStyles/Responsive/BoxedH4';
import Loading from '../../Loading/Loading';

const LettersPage = () => {
    const dispatch = useDispatch();
    const letters = useSelector((state) => state.letters);
    useEffect(() => {
        dispatch(fetchLetters());
    }, []); // eslint-disable-line react-hooks/exhaustive-deps
    return (
        <PageContainer>
            <Wrapper>
                <Header>
                    <BoxedH4>A - Z Artist list</BoxedH4>
                </Header>
                {letters.loading ? (
                    <Loading />
                ) : (
                    <Letters>
                        {letters.letters.map((letter) => (
                            <Letter letter={letter} key={letter} />
                        ))}
                    </Letters>
                )}
            </Wrapper>
        </PageContainer>
    );
};

const Wrapper = styled.div`
    display: grid;
    gap: 24px 0px;
`;

const Header = styled.div`
    /* background-color: red; */
    width: 100%;
`;

const Letters = styled.div`
    /* background-color: green; */

    display: grid;
    grid-template-columns: repeat(12, 84px);
    gap: 32px;

    @media (max-width: calc(1360px + 2 * 32px)) {
        grid-template-columns: repeat(4, 84px);
        gap: 16px;
    }
`;

export default LettersPage;
