import React from 'react';

import PageContainer from './../../PageContainer/PageContainer';

const AboutPage = () => {
    return (
        <PageContainer>
            <h1>About</h1>
        </PageContainer>
    );
};

export default AboutPage;
