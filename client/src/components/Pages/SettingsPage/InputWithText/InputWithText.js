import React from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

import Text from './../../../TextStyles/Responsive/ResponsivePSmall1';

const InputWithText = ({
    text,
    onChange = () => {},
    placeholder,
    type,
    value = '',
}) => {
    const currentTheme = useSelector((state) => state.themes.currentTheme);
    return (
        <Wrapper>
            <Text>{text}</Text>
            <InputBox
                background={currentTheme.settingsWidgetDark}
                borderColor={currentTheme.widgetBlue1}
                textColor={currentTheme.textWhite}
                placeholder={placeholder}
                type={type}
                value={value}
                onChange={onChange}
            />
        </Wrapper>
    );
};

const Wrapper = styled.div`
    /* background: green; */
    display: grid;
    grid-template-columns: auto 1fr;
    align-items: center;
    gap: 0px 8px;
`;

const InputBox = styled.input`
    background: ${(props) => props.background};
    border: 1px solid ${(props) => props.borderColor};
    color: ${(props) => props.textColor};
    height: 48px;
    border-radius: 8px;
`;

export default InputWithText;
