import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { toggleLoginModal } from '../../../reduxFiles/loginModal/actionCreators';

import PageContainer from './../../PageContainer/PageContainer';
import SettingsBox from './SettingsBox/SettingsBox';

const SettingsPage = () => {
    const user = useSelector((state) => state.user);
    const dispatch = useDispatch();
    const history = useHistory();
    if (!user.isAuthenticated) {
        dispatch(toggleLoginModal());
        history.push('/');
    }
    return (
        <PageContainer>
            <SettingsBox />
        </PageContainer>
    );
};

export default SettingsPage;
