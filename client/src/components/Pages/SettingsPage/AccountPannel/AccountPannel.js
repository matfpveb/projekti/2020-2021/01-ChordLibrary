import React from 'react';
import { useSelector } from 'react-redux';

import styled from 'styled-components';

import ResponsivePBodyIntro from './../../../TextStyles/Responsive/ResponsivePBodyIntro';
import ResponsivePBodyMain from './../../../TextStyles/Responsive/ResponsivePBodyMain';
import DeleteAccountButton from './../Buttons/DeleteAccountButton';
import ResetPasswordButton from './../Buttons/ResetPasswordButton';
import InputWithText from './../InputWithText/InputWithText';
import AvatarEditor from './../AvatarEditor/AvatarEditor';

const AccountPannel = () => {
    const currentTheme = useSelector((state) => state.themes.currentTheme);
    return (
        <Wrapper>
            <Box background={currentTheme.settingsWidgetDark}>
                <ResponsivePBodyIntro>Account</ResponsivePBodyIntro>
                <SettingsSection>
                    <ResponsivePBodyMain>
                        Personal informations
                    </ResponsivePBodyMain>
                    <InputWithText text='User name:' />
                    <InputWithText text='Email:' />
                    <AvatarEditor />
                </SettingsSection>
                <SettingsSection>
                    <ResponsivePBodyMain>
                        Password and authentication
                    </ResponsivePBodyMain>
                    <ResetPasswordButtonOverride />
                </SettingsSection>
                <SettingsSection>
                    <ResponsivePBodyMain>Account removal</ResponsivePBodyMain>
                    <DeleteAccountButtonOverride />
                </SettingsSection>
            </Box>
        </Wrapper>
    );
};

const Wrapper = styled.div`
    padding-top: 32px;
    padding-bottom: 32px;
    grid-column: span 5;

    @media (max-width: calc(1360px + 2 * 32px)) {
        grid-column: span 4;
    }
`;

const Box = styled.div`
    background: ${(props) => props.background};
    padding: 16px;
    border-radius: 10px;

    display: grid;
    /* grid-template-rows: max-content max-content max-content max-content; */
    gap: 24px 0px;
`;

const SettingsSection = styled.div`
    display: grid;
    /* grid-template-columns: min-content; */
    /* justify-items: flex-start; */
    gap: 16px 0px;
`;

const DeleteAccountButtonOverride = styled(DeleteAccountButton)`
    justify-self: flex-start;
`;

const ResetPasswordButtonOverride = styled(ResetPasswordButton)`
    justify-self: flex-start;
`;

export default AccountPannel;
