import React from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import SettingsButton from './SettingsButton/SettingsButton';

const AddNewSongButton = ({ ...otherProps }) => {
    const history = useHistory();
    const currentTheme = useSelector((state) => state.themes.currentTheme);

    return (
        <SettingsButton
            type='New'
            text='Add New Song'
            background={currentTheme.purpleGradientActive2}
            hoverBackground={currentTheme.purpleGradientActive}
            // border={`1px solid ${currentTheme.purple2}`}
            onClickHandler={() => {
                history.push('/add');
            }}
            {...otherProps}
        />
    );
};

export default AddNewSongButton;
