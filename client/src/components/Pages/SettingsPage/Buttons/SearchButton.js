import React from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import SettingsButton from './SettingsButton/SettingsButton';

const AddNewSongButton = ({ ...otherProps }) => {
    const history = useHistory();
    const currentTheme = useSelector((state) => state.themes.currentTheme);

    return (
        <SettingsButton
            type='Search'
            text='Search'
            background='none'
            onClickHandler={() => {
                history.push('/');
            }}
            {...otherProps}
        />
    );
};

export default AddNewSongButton;
