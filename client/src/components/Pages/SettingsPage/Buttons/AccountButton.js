import React from 'react';
import SettingsButton from './SettingsButton/SettingsButton';

const AccountButton = ({ onClickHandler, ...otherProps }) => {
    return (
        <SettingsButton
            type='Account'
            text='Account'
            onClickHandler={onClickHandler}
            {...otherProps}
        />
    );
};

export default AccountButton;
