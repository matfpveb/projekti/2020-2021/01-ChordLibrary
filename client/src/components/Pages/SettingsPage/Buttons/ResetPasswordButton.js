import React from 'react';
import { useSelector } from 'react-redux';
import SettingsButton from './SettingsButton/SettingsButton';

const ResetPasswordButton = ({ onClickHandler, ...otherProps }) => {
    const currentTheme = useSelector((state) => state.themes.currentTheme);
    return (
        <SettingsButton
            type='PasswordKey'
            background={currentTheme.mainBg2}
            text='Reset Password'
            onClickHandler={onClickHandler}
            {...otherProps}
        />
    );
};

export default ResetPasswordButton;
