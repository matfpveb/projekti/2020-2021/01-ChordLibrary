import React from 'react';
import { useHistory } from 'react-router-dom';
import SettingsButton from './SettingsButton/SettingsButton';

const AddNewSongButton = ({ ...otherProps }) => {
    const history = useHistory();

    return (
        <SettingsButton
            type='MusicNote'
            text='Artists'
            background='none'
            onClickHandler={() => {
                history.push('/letters');
            }}
            {...otherProps}
        />
    );
};

export default AddNewSongButton;
