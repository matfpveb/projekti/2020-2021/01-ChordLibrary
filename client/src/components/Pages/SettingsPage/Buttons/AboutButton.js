import React from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import SettingsButton from './SettingsButton/SettingsButton';

const AddNewSongButton = ({ background, hoverBackground, ...otherProps }) => {
    const history = useHistory();
    const currentTheme = useSelector((state) => state.themes.currentTheme);
    background = background || currentTheme.settingsWidgetLight;
    hoverBackground = hoverBackground || currentTheme.settingsWidgetLight;

    return (
        <SettingsButton
            type='About'
            text='About'
            background={background}
            hoverBackground={hoverBackground}
            onClickHandler={() => {
                history.push('/about');
            }}
            {...otherProps}
        />
    );
};

export default AddNewSongButton;
