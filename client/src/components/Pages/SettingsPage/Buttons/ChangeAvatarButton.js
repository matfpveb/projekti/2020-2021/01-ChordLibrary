import React from 'react';
import SettingsButton from './SettingsButton/SettingsButton';

const ChangeAvatarButton = ({ onClickHandler, ...otherProps }) => {
    return (
        <SettingsButton
            type='SettingsWheel'
            text='Change Avatar'
            onClickHandler={onClickHandler}
            {...otherProps}
        />
    );
};

export default ChangeAvatarButton;
