import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { userCardDropDownClose } from '../../../../reduxFiles/userCardDropDown/actionCreators';
import SettingsButton from './SettingsButton/SettingsButton';

const GoToSettingsButton = ({ background, hoverBackground, ...otherProps }) => {
    const history = useHistory();
    const dispatch = useDispatch();
    const currentTheme = useSelector((state) => state.themes.currentTheme);
    background = background || currentTheme.settingsWidgetLight;
    hoverBackground = hoverBackground || currentTheme.settingsWidgetLight;
    return (
        <SettingsButton
            type='SettingsSwitch'
            text='Settings'
            background={background}
            hoverBackground={hoverBackground}
            onClickHandler={() => {
                dispatch(userCardDropDownClose());
                history.push('/settings');
            }}
            {...otherProps}
        />
    );
};

export default GoToSettingsButton;
