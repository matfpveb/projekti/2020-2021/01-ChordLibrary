import React from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

import AboutIcon from '../../../../Icons/AboutIcon';
import AccountIcon from '../../../../Icons/AccountIcon';
import HomeIcon from '../../../../Icons/HomeIcon';
import LeftArrowIcon from '../../../../Icons/LeftArrowIcon';
import LogOutIcon from '../../../../Icons/LogOutIcon';
import MusicFileOutlineIcon from '../../../../Icons/MusicFileOutlineIcon';
import MusicFolderOutlineIcon from '../../../../Icons/MusicFolderOutlineIcon';
import MusicNoteIcon from '../../../../Icons/MusicNoteIcon';
import MusicPlaylistIcon from '../../../../Icons/MusicPlaylistIcon';
import NewIcon from './../../../../Icons/NewIcon';
import PasswordKeyIcon from '../../../../Icons/PasswordKeyIcon';
import RightArrowIcon from '../../../../Icons/RightArrowIcon';
import SearchIcon from '../../../../Icons/SearchIcon';
import SettingsSwitchIcon from '../../../../Icons/SettingsSwitchIcon';
import SettingsWheelIcon from '../../../../Icons/SettingsWheelIcon';
import TrashIcon from '../../../../Icons/TrashIcon';
import WarningIcon from '../../../../Icons/WarningIcon';
import LogInIcon from '../../../../Icons/LogInIcon';
import RegisterIcon from '../../../../Icons/RegisterIcon';
import ResponsivePSmall1 from '../../../../TextStyles/Responsive/ResponsivePSmall1';

const SettingsButton = ({
    text,
    type,
    hasIcon = true,
    hasText = true,
    background,
    hoverBackground,
    border,
    textColor,
    onClickHandler = () => {},
    ...otherProps
}) => {
    const currentTheme = useSelector((state) => state.themes.currentTheme);
    background = background || currentTheme.settingsWidgetLight;
    border = border || 'none';
    textColor = textColor || currentTheme.textWhite;
    hoverBackground = hoverBackground || background;
    const chooseIcon = () => {
        switch (type) {
            case 'About':
                return <AboutIcon />;
            case 'Account':
                return <AccountIcon />;
            case 'Home':
                return <HomeIcon />;
            case 'LogOut':
                return <LogOutIcon />;
            case 'LogIn':
                return <LogInIcon />;
            case 'Register':
                return <RegisterIcon />;
            case 'MusicFileOutline':
                return <MusicFileOutlineIcon />;
            case 'MusicFolderOutline':
                return <MusicFolderOutlineIcon />;
            case 'MusicNote':
                return <MusicNoteIcon />;
            case 'MusicPlaylist':
                return <MusicPlaylistIcon />;
            case 'New':
                return <NewIcon />;
            case 'PasswordKey':
                return <PasswordKeyIcon />;
            case 'RightArrow':
                return <RightArrowIcon />;
            case 'LeftArrow':
                return <LeftArrowIcon />;
            case 'Search':
                return <SearchIcon />;
            case 'SettingsSwitch':
                return <SettingsSwitchIcon />;
            case 'SettingsWheel':
                return <SettingsWheelIcon />;
            case 'Trash':
                return <TrashIcon />;
            case 'Warning':
                return <WarningIcon />;
            default:
                return <HomeIcon />;
        }
    };
    return (
        <Wrapper
            background={background}
            hoverBackground={hoverBackground}
            onClick={onClickHandler}
            border={border}
            textColor={textColor}
            {...otherProps}
        >
            {hasIcon && chooseIcon()}
            {hasText && <ResponsivePSmall1>{text}</ResponsivePSmall1>}
        </Wrapper>
    );
};

const Wrapper = styled.div`
    background: ${(props) => props.background};
    padding: 12px 16px;
    height: 48px;
    border-radius: 8px;
    border: ${(props) => props.border};
    color: ${(props) => props.textColor};

    display: grid;
    grid-template-columns: repeat(2, max-content);
    align-items: center;
    gap: 8px;

    &:hover {
        background: ${(props) => props.hoverBackground};
        cursor: pointer;
    }
`;

export default SettingsButton;
