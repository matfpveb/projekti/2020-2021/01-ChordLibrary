import React from 'react';
import { useSelector } from 'react-redux';
import SettingsButton from './SettingsButton/SettingsButton';

const SaveChangesButton = ({ onClickHandler, ...otherProps }) => {
    const currentTheme = useSelector((state) => state.themes.currentTheme);
    return (
        <SettingsButton
            hasIcon={false}
            background={currentTheme.purpleGradientActive}
            text='Save Changes'
            onClickHandler={onClickHandler}
            {...otherProps}
        />
    );
};

export default SaveChangesButton;
