import React from 'react';
import SettingsButton from './SettingsButton/SettingsButton';

const BackToMainSettingsMenuButton = ({ onClickHandler, ...otherProps }) => {
    return (
        <SettingsButton
            type='LeftArrow'
            text='Back to main settings menu'
            onClickHandler={onClickHandler}
            {...otherProps}
        />
    );
};

export default BackToMainSettingsMenuButton;
