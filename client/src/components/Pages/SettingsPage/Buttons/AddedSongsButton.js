import React from 'react';
import SettingsButton from './SettingsButton/SettingsButton';

const AddedSongsButton = ({ onClickHandler, ...otherProps }) => {
    return (
        <SettingsButton
            type='MusicPlaylist'
            text='Added Songs'
            onClickHandler={onClickHandler}
            {...otherProps}
        />
    );
};

export default AddedSongsButton;
