import React from 'react';
import { useSelector } from 'react-redux';
import SettingsButton from './SettingsButton/SettingsButton';

const RegisterButton = ({
    background,
    hoverBackground,
    onClickHandler,
    ...otherProps
}) => {
    const currentTheme = useSelector((state) => state.themes.currentTheme);
    background = background || currentTheme.settingsWidgetLight;
    hoverBackground = hoverBackground || currentTheme.settingsWidgetLight;
    onClickHandler = onClickHandler || (() => {});
    return (
        <SettingsButton
            type='Register'
            text='Make new account'
            background={background}
            hoverBackground={hoverBackground}
            onClickHandler={onClickHandler}
            {...otherProps}
        />
    );
};

export default RegisterButton;
