import React from 'react';
import { useHistory } from 'react-router-dom';
import SettingsButton from './SettingsButton/SettingsButton';

const AddNewSongButton = ({ ...otherProps }) => {
    const history = useHistory();

    return (
        <SettingsButton
            type='Home'
            text='Home'
            background='none'
            onClickHandler={() => {
                history.push('/');
            }}
            {...otherProps}
        />
    );
};

export default AddNewSongButton;
