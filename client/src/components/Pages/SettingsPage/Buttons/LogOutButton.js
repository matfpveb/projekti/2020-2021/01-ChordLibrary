import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import SettingsButton from './SettingsButton/SettingsButton';
import { logout } from './../../../../reduxFiles/user/actionCreators';
import { useHistory } from 'react-router-dom';
import { userCardDropDownClose } from '../../../../reduxFiles/userCardDropDown/actionCreators';

const LogOutButton = ({ background, hoverBackground, ...otherProps }) => {
    const dispatch = useDispatch();
    const currentTheme = useSelector((state) => state.themes.currentTheme);
    const history = useHistory();
    background = background || currentTheme.settingsWidgetLight;
    hoverBackground = hoverBackground || currentTheme.settingsWidgetLight;
    return (
        <SettingsButton
            type='LogOut'
            textColor={currentTheme.red}
            text='LogOut'
            background={background}
            hoverBackground={hoverBackground}
            onClickHandler={(event) => {
                event.preventDefault();
                dispatch(userCardDropDownClose());
                dispatch(logout());
                history.push('/');
            }}
            {...otherProps}
        />
    );
};

export default LogOutButton;
