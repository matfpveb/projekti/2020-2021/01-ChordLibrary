import React from 'react';
import { useSelector } from 'react-redux';
import SettingsButton from './SettingsButton/SettingsButton';

const DeleteAccountButton = ({ onClickHandler, ...otherProps }) => {
    const currentTheme = useSelector((state) => state.themes.currentTheme);
    return (
        <SettingsButton
            type='Trash'
            background='none'
            border={`1px solid ${currentTheme.red}`}
            textColor={currentTheme.red}
            text='Delete Account Forever'
            onClickHandler={onClickHandler}
            {...otherProps}
        />
    );
};

export default DeleteAccountButton;
