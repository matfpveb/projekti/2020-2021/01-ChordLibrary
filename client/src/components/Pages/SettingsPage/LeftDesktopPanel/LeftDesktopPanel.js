import React from 'react';
import styled from 'styled-components';

import AccountButton from './../Buttons/AccountButton';
import AddedSongsButton from './../Buttons/AddedSongsButton';
import LogOutButton from './../Buttons/LogOutButton';

const LeftDesktopPanel = () => {
    return (
        <Wrapper>
            <UpButtons>
                <AccountButton></AccountButton>
                <AddedSongsButton></AddedSongsButton>
            </UpButtons>
            <LogOutButton></LogOutButton>
        </Wrapper>
    );
};

const Wrapper = styled.div`
    grid-column: span 2;
    padding: 32px;
    padding-right: 0px;

    display: flex;
    flex-direction: column;
    justify-content: space-between;
`;

const UpButtons = styled.div`
    display: grid;
    grid-template-columns: 100%;
    gap: 16px 0px;
`;

export default LeftDesktopPanel;
