import React from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

import ChangeAvatarButton from './../Buttons/ChangeAvatarButton';
import Text from './../../../TextStyles/Responsive/ResponsivePBodyMain';
import Avatar from './../../../UserCard/Avatar/Avatar';

const AvatarEditor = () => {
    const currentTheme = useSelector((state) => state.themes.currentTheme);
    return (
        <Wrapper
            background={currentTheme.settingsWidgetDark}
            borderColor={currentTheme.widgetBlue1}
        >
            <AvatarAndUserName>
                <Avatar width='128px' />
                <Text>Lorem ipsum</Text>
            </AvatarAndUserName>
            <ChangeAvatarButton />
        </Wrapper>
    );
};

const Wrapper = styled.div`
    background: ${(props) => props.background};
    border: 1px solid ${(props) => props.borderColor};
    border-radius: 10px;
    height: 288px;

    display: grid;
    grid-template-rows: repeat(2, min-content);
    justify-items: center;
    align-content: center;
    gap: 16px;
`;

const AvatarAndUserName = styled.div`
    display: grid;
    gap: 8px;
`;

export default AvatarEditor;
