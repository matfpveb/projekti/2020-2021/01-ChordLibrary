import React from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

import HeaderText from '../../../TextStyles/Responsive/ResponsiveH3';
import AccountPannel from '../AccountPannel/AccountPannel';
import LeftDesktopPanel from '../LeftDesktopPanel/LeftDesktopPanel';
import RightDesktopPanel from './../RightDesktopPanel/RightDesktopPanel';
// import BackToMainSettingsMenuButton from './../Buttons/BackToMainSettingsMenuButton';

const SettingsBox = () => {
    const currentTheme = useSelector((state) => state.themes.currentTheme);
    const device = useSelector((state) => state.device);
    return (
        <Box background={currentTheme.widgetBlue1}>
            <Header background={currentTheme.widgetBlue2}>
                <TextContent>
                    <HeaderText>Settings</HeaderText>
                </TextContent>
            </Header>
            <Wrapper>
                {device.type === 'Desktop' && <LeftDesktopPanel />}
                {/* {device.type === 'Mobile' && <BackToMainSettingsMenuButton />} */}
                <AccountPannel />
                {device.type === 'Desktop' && <RightDesktopPanel />}
            </Wrapper>
        </Box>
    );
};

const Box = styled.div`
    background: ${(props) => props.background};

    border-radius: 30px;
    overflow: hidden;
    @media (max-width: calc(1360px + 2 * 32px)) {
    }
`;

const Header = styled.div`
    background: ${(props) => props.background};
    height: 120px;
    display: grid;
    align-items: center;
    padding-left: 24px;
`;

const TextContent = styled.div``;

const Wrapper = styled.div`
    display: grid;
    grid-template-columns: repeat(12, 84px);
    gap: 0px 32px;

    @media (max-width: calc(1360px + 2 * 32px)) {
        grid-template-columns: repeat(4, 84px);
        gap: 24px 16px;
    }
`;

export default SettingsBox;
