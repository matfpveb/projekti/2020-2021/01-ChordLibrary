import React from 'react';
import styled from 'styled-components';

import SettingsIllustration from './SettingsIllustration/SettingsIllustration';
import ResponsivePBodyMain from './../../../TextStyles/Responsive/ResponsivePBodyMain';
import SaveChangesButton from './../Buttons/SaveChangesButton';

const RightDesktopPanel = () => {
    return (
        <Wrapper>
            <Box>
                <TextAndIllustration>
                    <SettingsIllustration />

                    <Text>
                        On this page you can change how others see you, reset
                        password and delete account. “Save changes” button will
                        be active only if you make some changes in Personal
                        informations section.
                    </Text>
                </TextAndIllustration>

                <ButtonSection>
                    <SaveChangesButtonOverride />
                </ButtonSection>
            </Box>
        </Wrapper>
    );
};

const Wrapper = styled.div`
    /* background: rgba(255, 255, 255, 0.1); */
    padding: 32px;
    padding-left: 0;
    grid-column: span 5;
`;

const Box = styled.div`
    /* background: rgba(255, 255, 255, 0.1); */
    height: 100%;
    display: grid;
    grid-template-rows: auto 1fr;
`;

const TextAndIllustration = styled.div`
    /* background: rgba(255, 255, 255, 0.1); */
`;

const Text = styled(ResponsivePBodyMain)`
    text-align: center;
`;

const ButtonSection = styled.div`
    /* background: green; */
    display: grid;
`;

const SaveChangesButtonOverride = styled(SaveChangesButton)`
    align-self: flex-end;
    justify-self: flex-end;
`;

export default RightDesktopPanel;
