import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';

import { fetchArtist } from './../../../reduxFiles/artist/actionCreators';

import PageContainer from './../../PageContainer/PageContainer';
import ArtistSideBar from './../../ArtistSideBar/ArtistSideBar';
import SongCard from './../../SongCard/SongCard';
import ArtistCard from './../../ArtistCard/ArtistCard';
import Loading from '../../Loading/Loading';

const ArtistPage = () => {
    let { id } = useParams();
    const device = useSelector((state) => state.device);
    const artist = useSelector((state) => state.artist);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchArtist(id));
    }, []); // eslint-disable-line react-hooks/exhaustive-deps
    return (
        <PageContainer>
            {artist.loading ? (
                <Loading />
            ) : (
                <Wrapper>
                    {device.type === 'Desktop' ? (
                        <ArtistSideBar isAboutVisible={true} />
                    ) : (
                        <ArtistCard
                            artistName={artist.artistName}
                            artistID={artist.artistID}
                            numberOfSongs={artist.numberOfSongs}
                            force4ColumnSpan={true}
                        />
                    )}
                    <SongsList>
                        {artist.songs.map(({ songName, songID }) => (
                            <SongCard
                                artistName={artist.artistName}
                                songName={songName}
                                songID={songID}
                                transparent='Dark'
                                key={songID}
                            />
                        ))}
                    </SongsList>
                </Wrapper>
            )}
        </PageContainer>
    );
};

const Wrapper = styled.div`
    display: grid;

    grid-template-columns: repeat(12, 84px);
    gap: 0px 32px;

    @media (max-width: calc(1360px + 2 * 32px)) {
        grid-template-columns: repeat(4, 84px);
        gap: 32px 16px;
    }
`;

const SongsList = styled.div`
    /* background: rgba(255, 255, 255, 0.1); */
    grid-column: span 8;

    display: grid;

    grid-template-rows: repeat(5, min-content);
    gap: 16px 0px;

    @media (max-width: calc(1360px + 2 * 32px)) {
        grid-column: span 4;
    }
`;

export default ArtistPage;
