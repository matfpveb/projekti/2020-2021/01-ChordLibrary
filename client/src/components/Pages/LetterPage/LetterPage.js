import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import styled from 'styled-components';

import { fetchArtistsStartingWithLetter } from '../../../reduxFiles/artistsStartingWithLetter/actionCreators';

import PageContainer from './../../PageContainer/PageContainer';
import BoxedH4 from '../../TextStyles/Responsive/BoxedH4';
import Loading from '../../Loading/Loading';
import ArtistCard from '../../ArtistCard/ArtistCard';

const LetterPage = () => {
    let { letter } = useParams();
    const dispatch = useDispatch();
    const artists = useSelector((state) => state.artists);
    useEffect(() => {
        dispatch(fetchArtistsStartingWithLetter(letter));
    }, []); // eslint-disable-line react-hooks/exhaustive-deps
    return (
        <PageContainer>
            <Wrapper>
                <Header>
                    <BoxedH4>Letter {letter}</BoxedH4>
                </Header>
                {artists.loading ? (
                    <Loading />
                ) : (
                    <Artists>
                        {artists.artists.map(
                            ({ artistName, artistID, numberOfSongs }) => (
                                <ArtistCard
                                    artistName={artistName}
                                    artistID={artistID}
                                    numberOfSongs={numberOfSongs}
                                    key={artistID}
                                />
                            )
                        )}
                    </Artists>
                )}
            </Wrapper>
        </PageContainer>
    );
};

const Wrapper = styled.div`
    display: grid;
    gap: 24px 0px;
`;

const Header = styled.div`
    /* background-color: red; */
    width: 100%;
`;

const Artists = styled.div`
    /* background-color: green; */

    display: grid;
    grid-template-columns: repeat(12, 84px);
    gap: 32px;

    @media (max-width: calc(1360px + 2 * 32px)) {
        grid-template-columns: repeat(4, 84px);
        gap: 16px;
    }
`;

export default LetterPage;
