import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { toggleLoginModal } from '../../reduxFiles/loginModal/actionCreators';

const PrivateRoute = ({ children }) => {
    const user = useSelector((state) => state.user);
    const dispatch = useDispatch();
    const history = useHistory();

    useEffect(() => {
        if (!user.isAuthenticated && !user.loading) {
            dispatch(toggleLoginModal());
            history.push('/');
        }
    }, []); // eslint-disable-line react-hooks/exhaustive-deps
    return { ...children };
};

export default PrivateRoute;
