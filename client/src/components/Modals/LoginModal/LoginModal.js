import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import {
    toggleLoginModal,
    loginModalUserNameChanged,
    loginModalPasswordChanged,
    loginModalResetForm,
} from '../../../reduxFiles/loginModal/actionCreators';
import { login } from '../../../reduxFiles/user/actionCreators';
import InputWithText from './../../Pages/SettingsPage/InputWithText/InputWithText';
import LogInButton from './../../Pages/SettingsPage/Buttons/LogInButton';

const LoginModal = () => {
    const currentTheme = useSelector((state) => state.themes.currentTheme);
    const loginModal = useSelector((state) => state.loginModal);
    const dispatch = useDispatch();
    return (
        <Wrapper
            onClick={(event) => {
                if (event.target.id === 'loginWrapper') {
                    dispatch(toggleLoginModal());
                }
            }}
            id='loginWrapper'
        >
            <StyledForm
                background={currentTheme.widgetBlue1}
                onSubmit={(event) => {
                    event.preventDefault();
                }}
            >
                <h2>Login</h2>
                <div>
                    <InputWithText
                        text='Username:'
                        type='text'
                        value={loginModal.userName}
                        onChange={(event) => {
                            dispatch(
                                loginModalUserNameChanged(event.target.value)
                            );
                        }}
                    />
                </div>
                <div>
                    <InputWithText
                        text='Password:'
                        type='password'
                        value={loginModal.password}
                        onChange={(event) => {
                            dispatch(
                                loginModalPasswordChanged(event.target.value)
                            );
                        }}
                    />
                </div>

                <LogInButton
                    onClickHandler={() => {
                        const { userName, password } = loginModal;
                        dispatch(loginModalResetForm());
                        dispatch(login(userName, password));
                    }}
                />
            </StyledForm>
        </Wrapper>
    );
};

const Wrapper = styled.div`
    width: 100vw;
    height: 100vh;
    top: 0;
    left: 0;
    background: rgba(0, 0, 0, 0.5);
    position: fixed;
    display: grid;
    justify-items: center;
    align-items: flex-start;
    padding: 32px;
    z-index: 10;
    backdrop-filter: blur(4px);
`;

const StyledForm = styled.form`
    background: ${(props) => props.background};
    padding: 24px;
    border-radius: 8px;

    display: grid;
    gap: 16px;

    box-shadow: 4px 4px 64px 8px rgba(0, 0, 0, 0.8);
`;

export default LoginModal;
