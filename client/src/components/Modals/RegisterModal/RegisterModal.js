import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import {
    toggleRegisterModal,
    registerModalUsernameChanged,
    registerModalPasswordChanged,
    registerModalResetForm,
} from '../../../reduxFiles/registerModal/actionCreators';
import { register } from '../../../reduxFiles/user/actionCreators';
import InputWithText from './../../Pages/SettingsPage/InputWithText/InputWithText';
import RegisterButton from './../../Pages/SettingsPage/Buttons/RegisterButton';

const RegisterModal = () => {
    const currentTheme = useSelector((state) => state.themes.currentTheme);
    const dispatch = useDispatch();
    const registerModal = useSelector((state) => state.registerModal);
    return (
        <Wrapper
            onClick={(event) => {
                if (event.target.id === 'registerWrapper') {
                    dispatch(toggleRegisterModal());
                }
            }}
            id='registerWrapper'
        >
            <StyledForm
                background={currentTheme.widgetBlue1}
                onSubmit={(event) => {
                    event.preventDefault();
                    console.log('onSubmit');
                    dispatch(register());
                }}
            >
                <h2>Register</h2>
                <div>
                    <InputWithText
                        text='Username:'
                        type='text'
                        value={registerModal.userName}
                        onChange={(event) => {
                            dispatch(
                                registerModalUsernameChanged(event.target.value)
                            );
                        }}
                    />
                </div>
                <div>
                    <InputWithText
                        text='Password:'
                        type='password'
                        value={registerModal.password}
                        onChange={(event) => {
                            dispatch(
                                registerModalPasswordChanged(event.target.value)
                            );
                        }}
                    />
                </div>

                <RegisterButton
                    onClickHandler={() => {
                        const { userName, password } = registerModal;
                        dispatch(registerModalResetForm());
                        dispatch(register(userName, password));
                    }}
                />
            </StyledForm>
        </Wrapper>
    );
};

const Wrapper = styled.div`
    width: 100vw;
    height: 100vh;
    top: 0;
    left: 0;
    background: rgba(0, 0, 0, 0.5);
    position: fixed;
    display: grid;
    justify-items: center;
    align-items: flex-start;
    padding: 32px;
    z-index: 10;
    backdrop-filter: blur(4px);
`;

const StyledForm = styled.form`
    background: ${(props) => props.background};
    padding: 24px;
    border-radius: 8px;

    display: grid;
    gap: 16px;
    box-shadow: 4px 4px 64px 8px rgba(0, 0, 0, 0.8);
`;

export default RegisterModal;
