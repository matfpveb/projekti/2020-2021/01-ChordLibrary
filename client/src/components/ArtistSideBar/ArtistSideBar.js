import React from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

import ArtistCard from './../ArtistCard/ArtistCard';
import AboutHeaderText from './../TextStyles/Responsive/ResponsivePBodyMain';
import AboutParagraphText from './../TextStyles/Responsive/ResponsivePSmall2';
import SongCard from './../SongCard/SongCard';

const ArtistSideBar = ({ isAboutVisible }) => {
    const currentTheme = useSelector((state) => state.themes.currentTheme);
    const artist = useSelector((state) => state.artist);
    return (
        <Wrapper background={currentTheme.widgetBlue1}>
            <ArtistCard
                force4ColumnSpan={true}
                artistName={artist.artistName}
                artistID={artist.artistID}
                numberOfSongs={artist.numberOfSongs}
            />
            {isAboutVisible ? (
                <TextContent>
                    <AboutHeaderText>About</AboutHeaderText>
                    <AboutParagraphText>{artist.about}</AboutParagraphText>
                </TextContent>
            ) : (
                <SongsList>
                    {artist.songs.map(({ songName, songID }) => (
                        <SongCard
                            artistName={artist.artistName}
                            songName={songName}
                            songID={songID}
                            transparent='Light'
                            key={songID}
                        />
                    ))}
                </SongsList>
            )}
        </Wrapper>
    );
};

const Wrapper = styled.div`
    grid-column: 1 / span 4;

    background: ${(props) => props.background};

    border-radius: 30px;

    display: grid;
    grid-template-columns: repeat(4, 84px);
    grid-template-rows: repeat(2, min-content);
    gap: 16px 32px;
`;

const TextContent = styled.div`
    /* background: rgba(255, 255, 255, 0.1); */

    padding: 16px;

    padding-bottom: 32px;

    grid-column: 1 / span 4;

    display: grid;
    gap: 8px;
`;

const SongsList = styled.div`
    /* background: rgba(255, 255, 255, 0.1); */
    grid-column: 1 / -1;

    display: grid;
    gap: 16px 0px;
    padding: 24px 16px;
`;

export default ArtistSideBar;
