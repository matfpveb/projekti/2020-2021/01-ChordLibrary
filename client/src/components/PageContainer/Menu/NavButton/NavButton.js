import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import DesktopPSmall1 from '../../../TextStyles/DesktopPSmall1';

const NavButton = ({ text, linkTo }) => {
    const currentTheme = useSelector((state) => state.themes.currentTheme);
    return (
        <StyledButton
            $hoverBackground={currentTheme.settingsWidgetLight}
            to={linkTo}
        >
            <DesktopPSmall1>{text}</DesktopPSmall1>{' '}
        </StyledButton>
    );
};

const StyledButton = styled(Link)`
    background: none;
    border: none;
    height: 56px;
    border-radius: 8px;

    display: grid;
    align-items: center;
    justify-items: center;

    &:hover {
        background: ${(props) => props.$hoverBackground};
        cursor: pointer;
    }
`;

export default NavButton;
