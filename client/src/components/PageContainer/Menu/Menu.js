// Builtin and external imports

import React from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

// Internal imports

// Internal components

import DesktopNav from './DesktopNav/DesktopNav';
import HamburgerNav from './HamburgerNav/HamburgerNav';
import UserCard from '../../UserCard/UserCard';

// Component

const Menu = () => {
    const currentTheme = useSelector((state) => state.themes.currentTheme);
    const device = useSelector((state) => state.device);
    return (
        <StyledMenu background={currentTheme.widgetBlue1}>
            {device.type === 'Desktop' ? <DesktopNav /> : <HamburgerNav />}
            <UserCard />
        </StyledMenu>
    );
};

// Styled components

const StyledMenu = styled.div`
    background: ${(props) => props.background};
    grid-column: 1 / span 12;
    height: 80px;
    display: grid;
    padding-left: 16px;
    padding-right: 16px;
    grid-template-columns: auto auto;
    justify-content: space-between;
    align-items: center;
    border-radius: 0 0 16px 16px;
    /* Idea for animation */
    /* transform-origin: 50vw 0;
    transform: scale3d(0.7, 0.7, 0.7);
    opacity: 0.5;
    &:hover {
        transform: none;
        opacity: 1;
    } */

    @media (max-width: calc(1360px + 2 * 32px)) {
        grid-column: 1 / span 4;
        height: 56px;
    }
`;

export default Menu;
