import React from 'react';
import styled from 'styled-components';

import HomeButton from './../../../Pages/SettingsPage/Buttons/HomeButton';
// import SearchButton from './../../../Pages/SettingsPage/Buttons/SearchButton';
import ArtistsButton from './../../../Pages/SettingsPage/Buttons/ArtistsButton';
import AddNewSongButton from './../../../Pages/SettingsPage/Buttons/AddNewSongButton';

const DesktopNav = () => {
    return (
        <StyledNav>
            <HomeButton />
            <ArtistsButton />
            {/* <SearchButton /> */}
            <AddNewSongButton />
        </StyledNav>
    );
};

// Styled Components

const StyledNav = styled.div`
    /* background: red; */
    /* display: grid;
    grid-template-columns: repeat(4, 84px);
    gap: 0px 32px; */

    display: grid;
    grid-template-columns: repeat(5, min-content);
    gap: 0px 16px;
`;

export default DesktopNav;
