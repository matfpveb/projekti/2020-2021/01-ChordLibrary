import React from 'react';
import { useEffect } from 'react';
import styled from 'styled-components';
import { useSelector, useDispatch } from 'react-redux';

import makeOnWindowResizeHandler from '../../helperFunctions/makeOnWindowResizeHandler';

import Menu from './../PageContainer/Menu/Menu';
import LoginModal from './../Modals/LoginModal/LoginModal';
import RegisterModal from '../Modals/RegisterModal/RegisterModal';

const PageContainer = ({ children }) => {
    const currentTheme = useSelector((state) => state.themes.currentTheme);
    const device = useSelector((state) => state.device);
    const loginModal = useSelector((state) => state.loginModal);
    const registerModal = useSelector((state) => state.registerModal);
    const dispatch = useDispatch();
    useEffect(() => {
        window.addEventListener(
            'resize',
            makeOnWindowResizeHandler(dispatch, device, 100)
        );
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    return (
        <Page
            backgroundColor={currentTheme.mainBg3}
            color={currentTheme.textWhite}
        >
            <Wrapper backgroundColor={currentTheme.widgetBlue2}>
                {loginModal.isOpen && <LoginModal />}
                {registerModal.isOpen && <RegisterModal />}
                <Menu />
                <PageContent>{children}</PageContent>
            </Wrapper>
        </Page>
    );
};

// Styled Components

const Page = styled.div`
    background: ${(props) => props.backgroundColor};
    color: ${(props) => props.color};
    min-height: 100%;
    display: flex;
    justify-content: center;
    /* padding-top: calc(4px + 8px); */
    padding-bottom: 24px;

    @media (max-width: calc(1360px + 2 * 32px)) {
        padding-top: 16px;
        padding-bottom: 16px;
    }
`;

const Wrapper = styled.div`
    /* background: ${(props) => props.backgroundColor}; */
    width: 1360px;
    display: grid;
    /* padding-top: calc(4px + 8px); */
    grid-template-columns: repeat(12, 84px);
    grid-template-rows: auto 1fr;
    gap: 24px 32px;

    @media (max-width: calc(1360px + 2 * 32px)) {
        width: calc(4 * 84px + 3 * 16px);

        grid-template-columns: repeat(4, 84px);
        gap: 16px 16px;
    }
`;

const PageContent = styled.div`
    grid-column: 1 / -1;
`;

export default PageContainer;
