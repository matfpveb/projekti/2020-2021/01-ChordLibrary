import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import ResponsivePBodyIntro from '../TextStyles/Responsive/ResponsivePBodyIntro';
import ResponsivePSmall3 from '../TextStyles/Responsive/ResponsivePSmall3';

const ArtistCard = ({
    artistName,
    artistID,
    numberOfSongs,
    force4ColumnSpan = false,
}) => {
    const currentTheme = useSelector((state) => state.themes.currentTheme);
    const device = useSelector((state) => state.device);

    let gridColumnSpan = 2;
    if (force4ColumnSpan || device.type === 'Mobile') {
        gridColumnSpan = 4;
    } else if (device.type === 'Desktop') {
        if (artistName.length > 11 && artistName.length <= 22) {
            gridColumnSpan = 3;
        } else if (artistName.length > 22) {
            gridColumnSpan = 4;
        }
    }

    return (
        <Wrapper
            $background={currentTheme.purpleGradientActive}
            $gridColumnSpan={gridColumnSpan}
            to={`/artist/${artistID}`}
        >
            <ImageContainer />
            <TextContent>
                <ResponsivePBodyIntro>{artistName}</ResponsivePBodyIntro>
                <ResponsivePSmall3>
                    {numberOfSongs} song{numberOfSongs > 1 ? 's' : ''}
                </ResponsivePSmall3>
            </TextContent>
        </Wrapper>
    );
};

const Wrapper = styled(Link)`
    background: ${(props) => props.$background};
    grid-column: span ${(props) => props.$gridColumnSpan};
    height: 255px;

    display: grid;
    justify-content: center;
    align-items: center;
    justify-items: center;
    align-content: center;
    gap: 16px 0px;

    border-radius: 30px;
`;

const TextContent = styled.div`
    display: grid;
    justify-items: center;
`;

const ImageContainer = styled.div`
    background: rgba(255, 255, 255, 0.2);

    width: 130px;
    height: 130px;

    border-radius: 20px;
`;

export default ArtistCard;
