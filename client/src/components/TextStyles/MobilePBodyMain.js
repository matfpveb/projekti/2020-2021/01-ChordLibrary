import React from 'react';
import styled from 'styled-components';

const MobilePBodyMain = ({ children, ...other }) => {
    return <Text {...other}>{children}</Text>;
};

const Text = styled.p`
    font-family: Montserrat;
    font-style: normal;
    font-weight: normal;
    font-size: 17px;
    line-height: 21px;
`;

export default MobilePBodyMain;
