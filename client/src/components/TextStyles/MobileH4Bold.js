import React from 'react';
import styled from 'styled-components';

const MobileH4Bold = ({ children }) => {
    return <Text>{children}</Text>;
};

const Text = styled.h4`
    font-family: Montserrat;
    font-style: normal;
    font-weight: bold;
    font-size: 24px;
    line-height: 29px;
`;

export default MobileH4Bold;
