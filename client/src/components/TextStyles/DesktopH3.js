import React from 'react';
import styled from 'styled-components';

const DesktopH3 = ({ children }) => {
    return <Text>{children}</Text>;
};

const Text = styled.h3`
    font-family: Montserrat;
    font-style: normal;
    font-weight: bold;
    font-size: 40px;
    line-height: 49px;
`;

export default DesktopH3;
