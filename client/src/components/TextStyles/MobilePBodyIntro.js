import React from 'react';
import styled from 'styled-components';

const MobilePBodyIntro = ({ children }) => {
    return <Text>{children}</Text>;
};

const Text = styled.p`
    font-family: Montserrat;
    font-style: normal;
    font-weight: 500;
    font-size: 20px;
    line-height: 24px;
`;

export default MobilePBodyIntro;
