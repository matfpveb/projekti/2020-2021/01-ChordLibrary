import React from 'react';
import styled from 'styled-components';

const DesktopH1 = ({ children }) => {
    return <Text>{children}</Text>;
};

const Text = styled.h1`
    font-family: Montserrat;
    font-style: normal;
    font-weight: bold;
    font-size: 72px;
    line-height: 88px;
`;

export default DesktopH1;
