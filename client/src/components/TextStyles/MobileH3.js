import React from 'react';
import styled from 'styled-components';

const MobileH3 = ({ children }) => {
    return <Text>{children}</Text>;
};

const Text = styled.h3`
    font-family: Montserrat;
    font-style: normal;
    font-weight: bold;
    font-size: 28px;
    line-height: 34px;
`;

export default MobileH3;
