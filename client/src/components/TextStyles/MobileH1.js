import React from 'react';
import styled from 'styled-components';

const MobileH1 = ({ children }) => {
    return <Text>{children}</Text>;
};

const Text = styled.h1`
    font-family: Montserrat;
    font-style: normal;
    font-weight: bold;
    font-size: 34px;
    line-height: 41px;
`;

export default MobileH1;
