import React from 'react';
import styled from 'styled-components';

const DesktopH2 = ({ children }) => {
    return <Text>{children}</Text>;
};

const Text = styled.h2`
    font-family: Montserrat;
    font-style: normal;
    font-weight: bold;
    font-size: 50px;
    line-height: 61px;
`;

export default DesktopH2;
