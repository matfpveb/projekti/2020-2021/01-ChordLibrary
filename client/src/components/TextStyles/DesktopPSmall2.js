import React from 'react';
import styled from 'styled-components';

const DesktopPSmall2 = ({ children }) => {
    return <Text>{children}</Text>;
};

const Text = styled.p`
    font-family: Montserrat;
    font-style: normal;
    font-weight: normal;
    font-size: 15px;
    line-height: 18px;
`;

export default DesktopPSmall2;
