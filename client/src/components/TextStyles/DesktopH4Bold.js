import React from 'react';
import styled from 'styled-components';

const DesktopH4Bold = ({ children }) => {
    return <Text>{children}</Text>;
};

const Text = styled.h4`
    font-family: Montserrat;
    font-style: normal;
    font-weight: bold;
    font-size: 30px;
    line-height: 37px;
`;

export default DesktopH4Bold;
