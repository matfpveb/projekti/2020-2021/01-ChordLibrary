import React from 'react';
import { useSelector } from 'react-redux';

import DesktopH2 from './../DesktopH2';
import MobileH2 from './../MobileH2';

const ResponsiveH2 = ({ children }) => {
    const device = useSelector((state) => state.device);
    return device.type === 'Desktop' ? (
        <DesktopH2>{children}</DesktopH2>
    ) : (
        <MobileH2>{children}</MobileH2>
    );
};

export default ResponsiveH2;
