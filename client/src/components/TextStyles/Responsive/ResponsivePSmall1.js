import React from 'react';
import { useSelector } from 'react-redux';

import DesktopPSmall1 from './../DesktopPSmall1';
import MobilePSmall1 from './../MobilePSmall1';

const ResponsivePSmall1 = ({ children }) => {
    const device = useSelector((state) => state.device);
    return device.type === 'Desktop' ? (
        <DesktopPSmall1>{children}</DesktopPSmall1>
    ) : (
        <MobilePSmall1>{children}</MobilePSmall1>
    );
};

export default ResponsivePSmall1;
