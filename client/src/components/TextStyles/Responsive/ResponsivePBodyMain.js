import React from 'react';
import { useSelector } from 'react-redux';

import DesktopPBodyMain from './../DesktopPBodyMain';
import MobilePBodyMain from './../MobilePBodyMain';

const ResponsivePBodyMain = ({ children, ...other }) => {
    const device = useSelector((state) => state.device);
    return device.type === 'Desktop' ? (
        <DesktopPBodyMain {...other}>{children}</DesktopPBodyMain>
    ) : (
        <MobilePBodyMain {...other}>{children}</MobilePBodyMain>
    );
};

export default ResponsivePBodyMain;
