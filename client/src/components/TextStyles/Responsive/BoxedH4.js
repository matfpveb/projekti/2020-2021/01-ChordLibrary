import React from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

import ResponsiveH4Bold from './ResponsiveH4Bold';

const BoxedH4 = ({ children }) => {
    const currentTheme = useSelector((state) => state.themes.currentTheme);
    return (
        <Wrapper background={currentTheme.widgetBlue2}>
            <ResponsiveH4Bold>{children}</ResponsiveH4Bold>
        </Wrapper>
    );
};

const Wrapper = styled.div`
    background: ${(props) => props.background};
    height: 96px;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    border-radius: 16px;
    padding-left: 32px;
    padding-right: 32px;
`;

export default BoxedH4;
