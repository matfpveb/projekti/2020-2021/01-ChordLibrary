import React from 'react';
import { useSelector } from 'react-redux';

import DesktopPSmall3 from '../DesktopPSmall3';
import MobilePSmall3 from '../MobilePSmall3';

const ResponsivePSmall3 = ({ children }) => {
    const device = useSelector((state) => state.device);
    return device.type === 'Desktop' ? (
        <DesktopPSmall3>{children}</DesktopPSmall3>
    ) : (
        <MobilePSmall3>{children}</MobilePSmall3>
    );
};

export default ResponsivePSmall3;
