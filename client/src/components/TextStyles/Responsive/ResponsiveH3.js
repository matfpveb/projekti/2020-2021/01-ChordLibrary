import React from 'react';
import { useSelector } from 'react-redux';

import DesktopH3 from './../DesktopH3';
import MobileH3 from './../MobileH3';

const ResponsiveH3 = ({ children }) => {
    const device = useSelector((state) => state.device);
    return device.type === 'Desktop' ? (
        <DesktopH3>{children}</DesktopH3>
    ) : (
        <MobileH3>{children}</MobileH3>
    );
};

export default ResponsiveH3;
