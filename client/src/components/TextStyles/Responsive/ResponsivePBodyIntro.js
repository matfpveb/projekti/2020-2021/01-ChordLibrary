import React from 'react';
import { useSelector } from 'react-redux';

import DesktopPBodyIntro from '../DesktopPBodyIntro';
import MobilePBodyIntro from '../MobilePBodyIntro';

const ResponsivePBodyIntro = ({ children }) => {
    const device = useSelector((state) => state.device);
    return device.type === 'Desktop' ? (
        <DesktopPBodyIntro>{children}</DesktopPBodyIntro>
    ) : (
        <MobilePBodyIntro>{children}</MobilePBodyIntro>
    );
};

export default ResponsivePBodyIntro;
