import React from 'react';
import { useSelector } from 'react-redux';

import DesktopH1 from './../DesktopH1';
import MobileH1 from './../MobileH1';

const ResponsiveH1 = ({ children }) => {
    const device = useSelector((state) => state.device);
    return device.type === 'Desktop' ? (
        <DesktopH1>{children}</DesktopH1>
    ) : (
        <MobileH1>{children}</MobileH1>
    );
};

export default ResponsiveH1;
