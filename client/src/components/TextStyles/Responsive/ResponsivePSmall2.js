import React from 'react';
import { useSelector } from 'react-redux';

import DesktopPSmall2 from './../DesktopPSmall2';
import MobilePSmall2 from './../MobilePSmall2';

const ResponsivePSmall2 = ({ children }) => {
    const device = useSelector((state) => state.device);
    return device.type === 'Desktop' ? (
        <DesktopPSmall2>{children}</DesktopPSmall2>
    ) : (
        <MobilePSmall2>{children}</MobilePSmall2>
    );
};

export default ResponsivePSmall2;
