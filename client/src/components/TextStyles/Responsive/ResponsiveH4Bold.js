import React from 'react';
import { useSelector } from 'react-redux';

import DesktopH4Bold from './../DesktopH4Bold';
import MobileH4Bold from './../MobileH4Bold';

const ResponsiveH4Bold = ({ children }) => {
    const device = useSelector((state) => state.device);
    return device.type === 'Desktop' ? (
        <DesktopH4Bold>{children}</DesktopH4Bold>
    ) : (
        <MobileH4Bold>{children}</MobileH4Bold>
    );
};

export default ResponsiveH4Bold;
