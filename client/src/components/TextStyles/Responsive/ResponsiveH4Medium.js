import React from 'react';
import { useSelector } from 'react-redux';

import DesktopH4Medium from './../DesktopH4Medium';
import MobileH4Medium from './../MobileH4Medium';

const ResponsiveH4Medium = ({ children }) => {
    const device = useSelector((state) => state.device);
    return device.type === 'Desktop' ? (
        <DesktopH4Medium>{children}</DesktopH4Medium>
    ) : (
        <MobileH4Medium>{children}</MobileH4Medium>
    );
};

export default ResponsiveH4Medium;
