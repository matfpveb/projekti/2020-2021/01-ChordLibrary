import React from 'react';
import styled from 'styled-components';

const DesktopPBodyMain = ({ children, ...other }) => {
    return <Text {...other}>{children}</Text>;
};

const Text = styled.p`
    font-family: Montserrat;
    font-style: normal;
    font-weight: normal;
    font-size: 20px;
    line-height: 24px;
`;

export default DesktopPBodyMain;
