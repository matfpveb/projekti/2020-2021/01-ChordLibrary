import React from 'react';
import styled from 'styled-components';

const MobileH4Medium = ({ children }) => {
    return <Text>{children}</Text>;
};

const Text = styled.h4`
    font-family: Montserrat;
    font-style: normal;
    font-weight: 500;
    font-size: 24px;
    line-height: 29px;
`;

export default MobileH4Medium;
