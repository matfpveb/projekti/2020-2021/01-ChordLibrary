import React from 'react';
import styled from 'styled-components';

const MobilePSmall1 = ({ children }) => {
    return <Text>{children}</Text>;
};

const Text = styled.p`
    font-family: Montserrat;
    font-style: normal;
    font-weight: normal;
    font-size: 17px;
    line-height: 21px;
`;

export default MobilePSmall1;
