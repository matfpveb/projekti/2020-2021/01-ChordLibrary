import React from 'react';
import styled from 'styled-components';

const MobileH2 = ({ children }) => {
    return <Text>{children}</Text>;
};

const Text = styled.h2`
    font-family: Montserrat;
    font-style: normal;
    font-weight: bold;
    font-size: 32px;
    line-height: 39px;
`;

export default MobileH2;
