import React from 'react';
import styled from 'styled-components';

const MobilePSmall3 = ({ children }) => {
    return <Text>{children}</Text>;
};

const Text = styled.p`
    font-family: Montserrat;
    font-style: normal;
    font-weight: normal;
    font-size: 13px;
    line-height: 16px;
`;

export default MobilePSmall3;
