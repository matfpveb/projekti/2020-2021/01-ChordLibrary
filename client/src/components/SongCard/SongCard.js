import React from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import ResponsivePBodyIntro from '../TextStyles/Responsive/ResponsivePBodyIntro';
import ResponsivePSmall3 from '../TextStyles/Responsive/ResponsivePSmall3';

const SongCard = ({
    artistName,
    songName,
    songID,
    hasImage,
    imageUrl,
    transparent,
}) => {
    const currentTheme = useSelector((state) => state.themes.currentTheme);
    let background = 'none';
    let hoverBackground = 'none';
    if (transparent === 'Dark') {
        background = currentTheme.purpleGradientInactive;
        hoverBackground = currentTheme.purpleGradientActive;
    } else if (transparent === 'Light') {
        background = currentTheme.purpleGradientInactive2;
        hoverBackground = currentTheme.purpleGradientActive;
    }

    return (
        <Card
            $background={background}
            $hoverBackground={hoverBackground}
            to={`/chords/${songID}`}
        >
            {hasImage && <ImageContainer />}
            <TextContainer>
                <ResponsivePBodyIntro>{songName}</ResponsivePBodyIntro>
                <ResponsivePSmall3>{artistName}</ResponsivePSmall3>
            </TextContainer>
        </Card>
    );
};

const Card = styled(Link)`
    background: ${(props) => props.$background};

    display: grid;
    grid-template-columns: auto 1fr;
    gap: 16px;

    padding: 16px;
    border-radius: 16px;

    &:hover {
        background: ${(props) => props.$hoverBackground};
        cursor: pointer;
    }
`;

const ImageContainer = styled.div`
    background-color: rgba(255, 255, 255, 0.2);
    width: 64px;
    height: 64px;
    border-radius: 8px;
`;

const TextContainer = styled.div`
    display: grid;
    align-content: center;
    gap: 0px 0px;
`;

export default SongCard;
