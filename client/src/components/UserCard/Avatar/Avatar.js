import React from 'react';
import { VscAccount } from 'react-icons/vsc';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

const Avatar = ({ width = '32px' }) => {
    const user = useSelector((state) => state.user);
    const currentTheme = useSelector((state) => state.themes.currentTheme);
    return (
        <Wrapper background={currentTheme.purple1} width={width}>
            {!user.isAuthenticated ? (
                <VscAccount size={width * 1.22} />
            ) : (
                <UserImage width={width} />
            )}
        </Wrapper>
    );
};

const Wrapper = styled.div`
    background: ${(props) => props.background};
    /* background: green; */
    width: ${(props) => props.width};
    height: ${(props) => props.width};

    border-radius: 500px;
    overflow: hidden;

    display: grid;
    align-items: center;
    justify-items: center;
    justify-content: center;
    align-content: center;
`;

const UserImage = styled.div`
    width: ${(props) => props.width};
    height: ${(props) => props.width};
    background: no-repeat center/100%
        url('https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=80');
    /* background: red; */
`;

export default Avatar;
