import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';

import Text from './../TextStyles/Responsive/ResponsivePSmall1';
import LogOutButton from '../Pages/SettingsPage/Buttons/LogOutButton';
import GoToSettingsButton from '../Pages/SettingsPage/Buttons/GoToSettingsButton';
import {
    userCardDropDownClose,
    userCardDropDownOpen,
} from '../../reduxFiles/userCardDropDown/actionCreators';
import LogInButton from '../Pages/SettingsPage/Buttons/LogInButton';
import RegisterButton from '../Pages/SettingsPage/Buttons/RegisterButton';
import Avatar from './Avatar/Avatar';

import { toggleLoginModal } from '../../reduxFiles/loginModal/actionCreators';
import { toggleRegisterModal } from '../../reduxFiles/registerModal/actionCreators';

const UserCard = () => {
    const dispatch = useDispatch();
    const currentTheme = useSelector((state) => state.themes.currentTheme);
    const user = useSelector((state) => state.user);
    const { isOpen } = useSelector((state) => state.userCardDropDown);
    return (
        <Wrapper
            background='none'
            hoverBackground={currentTheme.settingsWidgetDark}
            onMouseEnter={() => dispatch(userCardDropDownOpen())}
            onMouseLeave={() => dispatch(userCardDropDownClose())}
        >
            <NameAndAvatarWrapper>
                <Text>
                    {user.isAuthenticated ? user.user.username : 'Guest'}
                </Text>
                <Avatar />
            </NameAndAvatarWrapper>
            {isOpen && (
                <>
                    <SpacerForKeepingHover />
                    <DropDownWrapper background={currentTheme.widgetBlue1}>
                        {user.isAuthenticated ? (
                            <>
                                <GoToSettingsButton
                                    background={currentTheme.settingsWidgetDark}
                                    hoverBackground={
                                        currentTheme.settingsWidgetLight
                                    }
                                />

                                <LogOutButton
                                    background={currentTheme.settingsWidgetDark}
                                    hoverBackground={
                                        currentTheme.settingsWidgetLight
                                    }
                                />
                            </>
                        ) : (
                            <>
                                <LogInButton
                                    background={currentTheme.settingsWidgetDark}
                                    hoverBackground={
                                        currentTheme.settingsWidgetLight
                                    }
                                    onClickHandler={() => {
                                        dispatch(userCardDropDownClose());
                                        dispatch(toggleLoginModal());
                                    }}
                                />
                                <RegisterButton
                                    background={currentTheme.settingsWidgetDark}
                                    hoverBackground={
                                        currentTheme.settingsWidgetLight
                                    }
                                    onClickHandler={() => {
                                        dispatch(userCardDropDownClose());
                                        dispatch(toggleRegisterModal());
                                    }}
                                />
                            </>
                        )}
                    </DropDownWrapper>
                </>
            )}
        </Wrapper>
    );
};

const Wrapper = styled.div`
    background: ${(props) => props.background};
    height: 56px;
    display: grid;
    justify-items: flex-end;
    align-items: center;
    padding-left: 16px;
    padding-right: 16px;
    border-radius: 16px;
    position: relative;
    &:hover {
        background: ${(props) => props.hoverBackground};
        cursor: pointer;
    }
`;

const SpacerForKeepingHover = styled.div`
    height: 24px;
    width: 100%;
    position: absolute;
    /* background: green; */
    top: 56px;
    right: 0;
    left: 0;
`;

const DropDownWrapper = styled.div`
    position: absolute;
    background: ${(props) => props.background};
    top: calc(56px + 24px);

    display: grid;
    gap: 16px;
    padding: 24px;
    border-radius: 8px;
`;

const NameAndAvatarWrapper = styled.div`
    display: grid;
    grid-template-columns: repeat(2, auto);
    justify-items: center;
    align-items: center;
    gap: 8px;
`;

export default UserCard;
